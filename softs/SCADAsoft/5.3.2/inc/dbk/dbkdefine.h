//-*-c++-*-
/******************************************************************************/
/*                                                                            */
/*  FILE             : dbkdefine.h                                            */
/*  FULL NAME                :                                                */
/*----------------------------------------------------------------------------*/
/*  COMPANY          : THALES IS                                              */
/*  CREATION DATE    : Mon May 05 17:49:59 2003                               */
/*  LANGUAGE         : C++                                                    */
/*............................................................................*/
/*  Copyright � THALES Information Systems 1996-2003.                         */
/*  All rights reserved.                                                      */
/*                                                                            */
/*  Unauthorized access, use, reproduction or distribution is prohibited.     */
/*............................................................................*/
/*  OVERVIEW                                                                  */
/*                                                                            */
/*............................................................................*/
/*  CONTENTS                                                                  */
/*                                                                            */
/******************************************************************************/
// IDENTIFICATION:
// $Id: $
//
// HISTORY:
// $Log: $


#ifndef __DBKDEFINE__
#define __DBKDEFINE__

#define DBK_HIERARCHY_NAME_LENGTH 32
#define DBK_HIERARCHY_ALIAS_LENGTH 64
#define DBK_ATTRIBUTENAME_LENGTH 64

#define DBK_CHILDREN_HASHTABLE_CAPACITY 37
#define DBK_ALIAS_HASHTABLE_CAPACITY 521
#define DBK_ATTRIBUTE_ARRAY_CAPACITY 64

#define DBK_NB_ITEMS_FOR_EMPTY_DATABASE_HASH 10
#define DBK_NB_CHILDREN_FOR_EMPTY_DATABASE_HASH 12



#define DBK_NB_ATTRIBUTES_FOR_EMPTY_DATABASE_HASH 100
#define DBK_NB_CLASS_FOR_EMPTY_DATABASE_HASH 10


#endif

