// 
//	    ILOG Views Studio 4.0 Command Description File
//		Grapher Extension
// 

command NewGrapherBuffer {
    label "&newGrapherBuffer";
    prompt "&newGrapherBufferP"
    category buffer;
    category grapher;
    bitmap "ivstudio/grapher/icgrapher.png"
    // this command execute the NewBuffer command
}

command SelectLinkImageMode {
    label "&sLinkImageM";
    prompt "&sLinkImageMP";
    bitmap "iclnm";
    category mode;
    category grapher;
}

command SelectOrientedLinkImageMode {
    label "&sOrientedLinkImageM";
    prompt "&sOrientedLinkImageMP";
    bitmap "icolnm";
    category mode;
    category grapher;
}

command SelectOneLinkImageMode {
    label "&sOneLinkImageM";
    prompt "&sOneLinkImageMP";
    bitmap "ic1lnm";
    category mode;
    category grapher;
}

command SelectOrientedOneLinkImageMode {
    label "&sOrientedOneLinkImageM";
    prompt "&sOrientedOneLinkImageMP";
    bitmap "ico1lnm";
    category mode;
    category grapher;
}

command SelectOneSplineLinkImageMode {
    label "&sOneSplineLinkImageM";
    prompt "&sOneSplineLinkImageMP";
    bitmap "ic1slnm";
    category mode;
    category grapher;
}

command SelectOrientedOneSplineLinkImageMode {
    label "&sOrientedOneSplineLinkImageM";
    prompt "&sOrientedOneSplineLinkImageMP";
    bitmap "ico1slnm";
    category mode;
    category grapher;
}

command SelectDoubleLinkImageMode {
    label "&sDoubleLinkImageM";
    prompt "&sDoubleLinkImageMP";
    bitmap "icdlnm";
    category mode;
    category grapher;
}

command SelectOrientedDoubleLinkImageMode {
    label "&sOrientedDoubleLinkImageM";
    prompt "&sOrientedDoubleLinkImageMP";
    bitmap "icodlnm";
    category mode;
    category grapher;
}

command SelectDoubleSplineLinkImageMode {
    label "&sDoubleSplineLinkImageM";
    prompt "&sDoubleSplineLinkImageMP";
    bitmap "icdslnm";
    category mode;
    category grapher;
}

command SelectOrientedDoubleSplineLinkImageMode {
    label "&sOrientedDoubleSplineLinkImageM";
    prompt "&sOrientedDoubleSplineLinkImageMP";
    bitmap "icodslnm";
    category mode;
    category grapher;
}

command SelectArcLinkImageMode {
    label "&sArcLinkImageM";
    prompt "&sArcLinkImageMP";
    bitmap "icdslnm";
    category mode;
    category grapher;
}

command SelectOrientedArcLinkImageMode {
    label "&sOrientedArcLinkImageM";
    prompt "&sOrientedArcLinkImageMP";
    bitmap "icdslnm";
    category mode;
    category grapher;
}

command SelectPolylineLinkImageMode {
    label "&sPolylineLinkImageM";
    prompt "&sPolylineLinkImageMP";
    bitmap "icdslnm";
    category mode;
    category grapher;
}

command SelectOrientedPolylineLinkImageMode {
    label "&sOrientedPolylineLinkImageM";
    prompt "&sOrientedPolylineLinkImageMP";
    bitmap "icdslnm";
    category mode;
    category grapher;
}

command MakeNode {
    label "&makeNode";
    prompt "&makeNodeP";
    bitmap "icmknode";
    category grapher;
}

command SelectPinEditorMode {
    label "&pinEditor";
    prompt "&pinEditorP";
    bitmap "icpinedit";
    category grapher;
}

command SelectSCGrapherRectangleMode {
    label "IlvSCGrapherRectangle";
    prompt "&scGrapherPrompt";
    bitmap "icscgr";
    category mode;
}
