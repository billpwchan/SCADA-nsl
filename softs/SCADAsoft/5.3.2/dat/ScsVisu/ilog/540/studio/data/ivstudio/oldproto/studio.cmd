command NewPrototypeGrapherBuffer {
    label  "&IlvMsgStProto139";
    prompt "&IlvMsgStProto140";
    category prototypes;
}

command NewPrototypeManagerBuffer {
    label  "&IlvMsgStProto051";
    prompt "&IlvMsgStProto136";
    category prototypes;
}

command NewPrototypeGadgetManagerBuffer {
    label  "&IlvMsgStProto052";
    prompt "&IlvMsgStProto138";
    category prototypes;
}

command AddValueSource {
    label  "&IlvMsgStProto187";
    prompt "&IlvMsgStProto188";
    category prototypes;
}

command DeleteValueSource {
    label  "&IlvMsgStProto189";
    prompt "&IlvMsgStProto190";
    category prototypes;
}

command StartValueSources {
    label  "&IlvMsgStProto200";
    prompt "&IlvMsgStProto201";
    category prototypes;
}

command SuspendValueSources {
    label  "&IlvMsgStProto202";
    prompt "&IlvMsgStProto203";
    category prototypes;
}

command StopValueSources {
    label  "&IlvMsgStProto204";
    prompt "&IlvMsgStProto205";
    category prototypes;
}

// Modes

command SelectGroupSelectionMode {
    label "&IlvMsgStProto173";
    prompt "&IlvMsgStProto174";
    bitmap "icgrpsel";
    category mode;
    category prototypes;
}

command SelectGroupGraphSelectionMode {
    label "&IlvMsgStProto175";
    prompt "&IlvMsgStProto176";
    bitmap "icgrpsel";
    category mode;
    category prototypes;
}

command SelectNodeSelectionMode {
    label "&IlvMsgStProto177";
    prompt "&IlvMsgStProto178";
    bitmap "icselect";
    category mode;
    category prototypes;
}

command ConvertProtoBuffer {
    label "&ConvertProtoBuffer";
    prompt "&ConvertProtoBufferPrompt";
    category prototypes;
}





