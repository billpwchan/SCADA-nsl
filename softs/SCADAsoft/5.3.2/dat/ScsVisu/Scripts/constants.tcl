namespace eval Const {}


#########################################
#
# ---- Scroll Bar
#
#########################################

set Const::SB_Show      1
set Const::SB_Hide	 2
set Const::SB_AsNeeded  4

#########################################
#
# ---- Window Styles 
#
#########################################

set Const::WS_Standard           0

set Const::WS_NoBorder           2
set Const::WS_NoResizeBorder     4
set Const::WS_SaveUnder          8
set Const::WS_NotActivated      16
set Const::WS_NoTitleBar        32
set Const::WS_NoSystemMenu      64
set Const::WS_NoCloseBox       128
set Const::WS_NoMinBox         256
set Const::WS_NoMaxBox         512
set Const::WS_NotInWinList    1024
set Const::WS_Iconified       2048
set Const::WS_Maximized       4096
set Const::WS_MDIChild        8192
set Const::WS_TopMost        16384

#########################################
#
# ---- Image Types 
#
#########################################

set Const::IT_Default 0
set Const::IT_Source  1
set Const::IT_Binary  2


