// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_IDL_RTECEVENTCOMMC_H_
#define _TAO_IDL_RTECEVENTCOMMC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "orbsvcs/Event/event_export.h"
#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/ORB.h"
#include "tao/SystemException.h"
#include "tao/Basic_Types.h"
#include "tao/ORB_Constants.h"
#include "tao/Object.h"
#include "tao/Sequence_T.h"
#include "tao/Objref_VarOut_T.h"
#include "tao/Seq_Var_T.h"
#include "tao/Seq_Out_T.h"
#include "tao/VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#include "TimeBaseC.h"
#include "RtecDefaultEventDataC.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO TAO_RTEvent_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from 
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root_ch.cpp:62

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



namespace TAO
{
  class Collocation_Proxy_Broker;
  template<typename T> class Narrow_Utils;
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::OctetSeq EventPayload;
typedef CORBA::OctetSeq_var EventPayload_var;
typedef CORBA::OctetSeq_out EventPayload_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace RtecEventComm
{
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:519
  
  typedef RtecEventData EventData;
  typedef RtecEventData_var EventData_var;
  typedef RtecEventData_out EventData_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_EventData;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
  
  typedef TimeBase::TimeT Time;
  typedef TimeBase::TimeT_out Time_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_Time;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
  
  typedef ::CORBA::Long EventSourceID;
  typedef ::CORBA::Long_out EventSourceID_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_EventSourceID;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
  
  typedef ::CORBA::Long EventType;
  typedef ::CORBA::Long_out EventType_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_EventType;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct EventHeader;
  
  typedef
    TAO_Fixed_Var_T<
        EventHeader
      >
    EventHeader_var;
  
  typedef
    EventHeader &
    EventHeader_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_RTEvent_Export EventHeader
  {
    typedef EventHeader_var _var_type;
    typedef EventHeader_out _out_type;
    
    static void _tao_any_destructor (void *);
    RtecEventComm::EventType type;
    RtecEventComm::EventSourceID source;
    ::CORBA::Long ttl;
    RtecEventComm::Time creation_time;
    RtecEventComm::Time ec_recv_time;
    RtecEventComm::Time ec_send_time;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_EventHeader;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct Event;
  
  typedef
    TAO_Var_Var_T<
        Event
      >
    Event_var;
  
  typedef
    TAO_Out_T<
        Event
      >
    Event_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_RTEvent_Export Event
  {
    typedef Event_var _var_type;
    typedef Event_out _out_type;
    
    static void _tao_any_destructor (void *);
    RtecEventComm::EventHeader header;
    RtecEventComm::EventData data;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_Event;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_RTECEVENTCOMM_EVENTSET_CH_)
#define _RTECEVENTCOMM_EVENTSET_CH_
  
  class EventSet;
  
  typedef
    TAO_VarSeq_Var_T<
        EventSet
      >
    EventSet_var;
  
  typedef
    TAO_Seq_Out_T<
        EventSet
      >
    EventSet_out;
  
  class TAO_RTEvent_Export EventSet
    : public
        TAO::unbounded_value_sequence<
            Event
          >
  {
  public:
    EventSet (void);
    EventSet ( ::CORBA::ULong max);
    EventSet (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        Event* buffer, 
        ::CORBA::Boolean release = false
      );
    EventSet (const EventSet &);
    virtual ~EventSet (void);
    
    static void _tao_any_destructor (void *);
    
    typedef EventSet_var _var_type;
    typedef EventSet_out _out_type;
    
    
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_EventSet;
  
  // TAO_IDL - Generated from
  // be\be_interface.cpp:644

#if !defined (_RTECEVENTCOMM_PUSHCONSUMER__VAR_OUT_CH_)
#define _RTECEVENTCOMM_PUSHCONSUMER__VAR_OUT_CH_
  
  class PushConsumer;
  typedef PushConsumer *PushConsumer_ptr;
  
  typedef
    TAO_Objref_Var_T<
        PushConsumer
      >
    PushConsumer_var;
  
  typedef
    TAO_Objref_Out_T<
        PushConsumer
      >
    PushConsumer_out;

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:54

#if !defined (_RTECEVENTCOMM_PUSHCONSUMER_CH_)
#define _RTECEVENTCOMM_PUSHCONSUMER_CH_
  
  class TAO_RTEvent_Export PushConsumer
    : public virtual ::CORBA::Object
  {
  public:
    friend class TAO::Narrow_Utils<PushConsumer>;
    typedef PushConsumer_ptr _ptr_type;
    typedef PushConsumer_var _var_type;
    typedef PushConsumer_out _out_type;
    
    // The static operations.
    static PushConsumer_ptr _duplicate (PushConsumer_ptr obj);
    
    static void _tao_release (PushConsumer_ptr obj);
    
    static PushConsumer_ptr _narrow (::CORBA::Object_ptr obj);
    static PushConsumer_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
    static PushConsumer_ptr _nil (void)
    {
      return static_cast<PushConsumer_ptr> (0);
    }
    
    static void _tao_any_destructor (void *);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void push (
        const ::RtecEventComm::EventSet & data);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void disconnect_push_consumer (
        void);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:216
    
    virtual ::CORBA::Boolean _is_a (const char *type_id);
    virtual const char* _interface_repository_id (void) const;
    virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
  private:
    TAO::Collocation_Proxy_Broker *the_TAO_PushConsumer_Proxy_Broker_;
  
  protected:
    // Concrete interface only.
    PushConsumer (void);
    
    // These methods travese the inheritance tree and set the
    // parents piece of the given class in the right mode.
    virtual void RtecEventComm_PushConsumer_setup_collocation (void);
    
    // Concrete non-local interface only.
    PushConsumer (
        ::IOP::IOR *ior,
        TAO_ORB_Core *orb_core);
    
    // Non-local interface only.
    PushConsumer (
        TAO_Stub *objref,
        ::CORBA::Boolean _tao_collocated = false,
        TAO_Abstract_ServantBase *servant = 0,
        TAO_ORB_Core *orb_core = 0);
    
    virtual ~PushConsumer (void);
  
  private:
    // Private and unimplemented for concrete interfaces.
    PushConsumer (const PushConsumer &);
    
    void operator= (const PushConsumer &);
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_PushConsumer;
  
  // TAO_IDL - Generated from
  // be\be_interface.cpp:644

#if !defined (_RTECEVENTCOMM_PUSHSUPPLIER__VAR_OUT_CH_)
#define _RTECEVENTCOMM_PUSHSUPPLIER__VAR_OUT_CH_
  
  class PushSupplier;
  typedef PushSupplier *PushSupplier_ptr;
  
  typedef
    TAO_Objref_Var_T<
        PushSupplier
      >
    PushSupplier_var;
  
  typedef
    TAO_Objref_Out_T<
        PushSupplier
      >
    PushSupplier_out;

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:54

#if !defined (_RTECEVENTCOMM_PUSHSUPPLIER_CH_)
#define _RTECEVENTCOMM_PUSHSUPPLIER_CH_
  
  class TAO_RTEvent_Export PushSupplier
    : public virtual ::CORBA::Object
  {
  public:
    friend class TAO::Narrow_Utils<PushSupplier>;
    typedef PushSupplier_ptr _ptr_type;
    typedef PushSupplier_var _var_type;
    typedef PushSupplier_out _out_type;
    
    // The static operations.
    static PushSupplier_ptr _duplicate (PushSupplier_ptr obj);
    
    static void _tao_release (PushSupplier_ptr obj);
    
    static PushSupplier_ptr _narrow (::CORBA::Object_ptr obj);
    static PushSupplier_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
    static PushSupplier_ptr _nil (void)
    {
      return static_cast<PushSupplier_ptr> (0);
    }
    
    static void _tao_any_destructor (void *);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void disconnect_push_supplier (
        void);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:216
    
    virtual ::CORBA::Boolean _is_a (const char *type_id);
    virtual const char* _interface_repository_id (void) const;
    virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
  private:
    TAO::Collocation_Proxy_Broker *the_TAO_PushSupplier_Proxy_Broker_;
  
  protected:
    // Concrete interface only.
    PushSupplier (void);
    
    // These methods travese the inheritance tree and set the
    // parents piece of the given class in the right mode.
    virtual void RtecEventComm_PushSupplier_setup_collocation (void);
    
    // Concrete non-local interface only.
    PushSupplier (
        ::IOP::IOR *ior,
        TAO_ORB_Core *orb_core);
    
    // Non-local interface only.
    PushSupplier (
        TAO_Stub *objref,
        ::CORBA::Boolean _tao_collocated = false,
        TAO_Abstract_ServantBase *servant = 0,
        TAO_ORB_Core *orb_core = 0);
    
    virtual ~PushSupplier (void);
  
  private:
    // Private and unimplemented for concrete interfaces.
    PushSupplier (const PushSupplier &);
    
    void operator= (const PushSupplier &);
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_RTEvent_Export ::CORBA::TypeCode_ptr const _tc_PushSupplier;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module RtecEventComm

// Proxy Broker Factory function pointer declarations.

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root.cpp:139

extern TAO_RTEvent_Export
TAO::Collocation_Proxy_Broker *
(*RtecEventComm__TAO_PushConsumer_Proxy_Broker_Factory_function_pointer) (
    ::CORBA::Object_ptr obj
  );

extern TAO_RTEvent_Export
TAO::Collocation_Proxy_Broker *
(*RtecEventComm__TAO_PushSupplier_Proxy_Broker_Factory_function_pointer) (
    ::CORBA::Object_ptr obj
  );

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{

#if !defined (_RTECEVENTCOMM_PUSHCONSUMER__TRAITS_)
#define _RTECEVENTCOMM_PUSHCONSUMER__TRAITS_
  
  template<>
  struct TAO_RTEvent_Export Objref_Traits< ::RtecEventComm::PushConsumer>
  {
    static ::RtecEventComm::PushConsumer_ptr duplicate (
        ::RtecEventComm::PushConsumer_ptr p
      );
    static void release (
        ::RtecEventComm::PushConsumer_ptr p
      );
    static ::RtecEventComm::PushConsumer_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::RtecEventComm::PushConsumer_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */

#if !defined (_RTECEVENTCOMM_PUSHSUPPLIER__TRAITS_)
#define _RTECEVENTCOMM_PUSHSUPPLIER__TRAITS_
  
  template<>
  struct TAO_RTEvent_Export Objref_Traits< ::RtecEventComm::PushSupplier>
  {
    static ::RtecEventComm::PushSupplier_ptr duplicate (
        ::RtecEventComm::PushSupplier_ptr p
      );
    static void release (
        ::RtecEventComm::PushSupplier_ptr p
      );
    static ::RtecEventComm::PushSupplier_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::RtecEventComm::PushSupplier_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export void operator<<= (::CORBA::Any &, const RtecEventComm::EventHeader &); // copying version
TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::EventHeader*); // noncopying version
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, RtecEventComm::EventHeader *&); // deprecated
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const RtecEventComm::EventHeader *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export void operator<<= (::CORBA::Any &, const RtecEventComm::Event &); // copying version
TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::Event*); // noncopying version
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, RtecEventComm::Event *&); // deprecated
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const RtecEventComm::Event *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, const RtecEventComm::EventSet &); // copying version
TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, RtecEventComm::EventSet*); // noncopying version
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, RtecEventComm::EventSet *&); // deprecated
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const RtecEventComm::EventSet *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/any_op_ch.cpp:54



#if defined (ACE_ANY_OPS_USE_NAMESPACE)

namespace RtecEventComm
{
  TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, PushConsumer_ptr); // copying
  TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, PushConsumer_ptr *); // non-copying
  TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, PushConsumer_ptr &);
}

#else



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::PushConsumer_ptr); // copying
TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::PushConsumer_ptr *); // non-copying
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, RtecEventComm::PushConsumer_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/any_op_ch.cpp:54



#if defined (ACE_ANY_OPS_USE_NAMESPACE)

namespace RtecEventComm
{
  TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, PushSupplier_ptr); // copying
  TAO_RTEvent_Export void operator<<= ( ::CORBA::Any &, PushSupplier_ptr *); // non-copying
  TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, PushSupplier_ptr &);
}

#else



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::PushSupplier_ptr); // copying
TAO_RTEvent_Export void operator<<= (::CORBA::Any &, RtecEventComm::PushSupplier_ptr *); // non-copying
TAO_RTEvent_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, RtecEventComm::PushSupplier_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const RtecEventComm::EventHeader &);
TAO_RTEvent_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, RtecEventComm::EventHeader &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const RtecEventComm::Event &);
TAO_RTEvent_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, RtecEventComm::Event &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_RtecEventComm_EventSet_H_
#define _TAO_CDR_OP_RtecEventComm_EventSet_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_RTEvent_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const RtecEventComm::EventSet &_tao_sequence
  );
TAO_RTEvent_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    RtecEventComm::EventSet &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_RtecEventComm_EventSet_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/cdr_op_ch.cpp:55


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const RtecEventComm::PushConsumer_ptr );
TAO_RTEvent_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, RtecEventComm::PushConsumer_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/cdr_op_ch.cpp:55


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_RTEvent_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const RtecEventComm::PushSupplier_ptr );
TAO_RTEvent_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, RtecEventComm::PushSupplier_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#if defined (__ACE_INLINE__)
#include "RtecEventCommC.inl"
#endif /* defined INLINE */

#include /**/ "ace/post.h"

#endif /* ifndef */


