/* -*- C++ -*- */
//
// $Id: be_visitor_constant.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_constant.h
//
// = DESCRIPTION
//    Concrete visitor for the constant class
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_CONSTANT_H
#define TAO_BE_VISITOR_CONSTANT_H

#include "be_visitor_decl.h"
#include "be_visitor_constant/constant_ch.h"
#include "be_visitor_constant/constant_cs.h"

#endif // TAO_BE_VISITOR_CONSTANT_H
