/* -*- c++ -*- */
//
// $Id: be_visitor_typedef.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_typedef.h
//
// = DESCRIPTION
//    Concrete visitor for the Typedef class
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_TYPEDEF_H
#define TAO_BE_VISITOR_TYPEDEF_H

#include "be_visitor_decl.h"
#include "be_visitor_typedef/typedef.h"
#include "be_visitor_typedef/typedef_ch.h"
#include "be_visitor_typedef/typedef_ci.h"
#include "be_visitor_typedef/typedef_cs.h"
#include "be_visitor_typedef/any_op_ch.h"
#include "be_visitor_typedef/any_op_cs.h"
#include "be_visitor_typedef/cdr_op_ch.h"
#include "be_visitor_typedef/cdr_op_cs.h"
#include "be_visitor_typedef/serializer_op_ch.h"
#include "be_visitor_typedef/serializer_op_cs.h"

#endif // TAO_BE_VISITOR_TYPEDEF_H
