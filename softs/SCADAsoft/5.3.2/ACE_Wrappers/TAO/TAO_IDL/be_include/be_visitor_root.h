/* -*- c++ -*- */
//
// $Id: be_visitor_root.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_root.h
//
// = DESCRIPTION
//    Concrete visitor for the Root class
//
// = AUTHOR
//    Aniruddha Gokhale and Carlos O'Ryan
//
// ============================================================================

#ifndef TAO_BE_VISITOR_ROOT_H
#define TAO_BE_VISITOR_ROOT_H

#include "be_visitor_scope.h"
#include "be_visitor_root/root.h"
#include "be_visitor_root/root_ch.h"
#include "be_visitor_root/root_ci.h"
#include "be_visitor_root/root_cs.h"
#include "be_visitor_root/root_sh.h"
#include "be_visitor_root/root_si.h"
#include "be_visitor_root/root_ss.h"
#include "be_visitor_root/root_sth.h"
#include "be_visitor_root/root_is.h"
#include "be_visitor_root/root_ih.h"
#include "be_visitor_root/any_op.h"
#include "be_visitor_root/cdr_op.h"
#include "be_visitor_root/serializer_op.h"

#endif // TAO_BE_VISITOR_ROOT_H
