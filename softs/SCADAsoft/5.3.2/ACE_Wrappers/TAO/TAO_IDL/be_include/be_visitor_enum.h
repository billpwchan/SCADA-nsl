/* -*- c++ -*- */
//
// $Id: be_visitor_enum.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_enum.h
//
// = DESCRIPTION
//    Concrete visitor for the enum class
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_ENUM_H
#define TAO_BE_VISITOR_ENUM_H

#include "be_visitor_scope.h"
#include "be_visitor_enum/enum_ch.h"
#include "be_visitor_enum/enum_cs.h"
#include "be_visitor_enum/any_op_ch.h"
#include "be_visitor_enum/any_op_cs.h"
#include "be_visitor_enum/cdr_op_ch.h"
#include "be_visitor_enum/cdr_op_cs.h"
#include "be_visitor_enum/serializer_op_ch.h"
#include "be_visitor_enum/serializer_op_cs.h"

#endif /* TAO_BE_VISITOR_ENUM_H */
