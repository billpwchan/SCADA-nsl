// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_PIDL_MESSAGING_MESSAGING_SYNCSCOPE_POLICYC_H_
#define _TAO_PIDL_MESSAGING_MESSAGING_SYNCSCOPE_POLICYC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "tao/Messaging/messaging_export.h"
#include "tao/Basic_Types.h"
#include "tao/Object.h"
#include "tao/Objref_VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#include "tao/PolicyC.h"
#include "tao/Messaging_SyncScopeC.h"
#include "tao/Messaging/Messaging_TypesC.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO TAO_Messaging_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace Messaging
{
  
  // TAO_IDL - Generated from
  // be\be_interface.cpp:644

#if !defined (_MESSAGING_SYNCSCOPEPOLICY__VAR_OUT_CH_)
#define _MESSAGING_SYNCSCOPEPOLICY__VAR_OUT_CH_
  
  class SyncScopePolicy;
  typedef SyncScopePolicy *SyncScopePolicy_ptr;
  
  typedef
    TAO_Objref_Var_T<
        SyncScopePolicy
      >
    SyncScopePolicy_var;
  
  typedef
    TAO_Objref_Out_T<
        SyncScopePolicy
      >
    SyncScopePolicy_out;

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:54

#if !defined (_MESSAGING_SYNCSCOPEPOLICY_CH_)
#define _MESSAGING_SYNCSCOPEPOLICY_CH_
  
  class TAO_Messaging_Export SyncScopePolicy
    : public virtual ::CORBA::Policy
  {
  public:
    typedef SyncScopePolicy_ptr _ptr_type;
    typedef SyncScopePolicy_var _var_type;
    typedef SyncScopePolicy_out _out_type;
    
    // The static operations.
    static SyncScopePolicy_ptr _duplicate (SyncScopePolicy_ptr obj);
    
    static void _tao_release (SyncScopePolicy_ptr obj);
    
    static SyncScopePolicy_ptr _narrow (::CORBA::Object_ptr obj);
    static SyncScopePolicy_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
    static SyncScopePolicy_ptr _nil (void)
    {
      return static_cast<SyncScopePolicy_ptr> (0);
    }
    
    
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual ::Messaging::SyncScope synchronization (
        void) = 0;
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual ::CORBA::Policy_ptr copy (
        void) = 0;
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void destroy (
        void) = 0;
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:216
    
    virtual ::CORBA::Boolean _is_a (const char *type_id);
    virtual const char* _interface_repository_id (void) const;
    virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
  
  protected:
    // Abstract or local interface only.
    SyncScopePolicy (void);
    
    virtual ~SyncScopePolicy (void);
  
  private:
    // Private and unimplemented for concrete interfaces.
    SyncScopePolicy (const SyncScopePolicy &);
    
    void operator= (const SyncScopePolicy &);
  };

#endif /* end #if !defined */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module Messaging

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{

#if !defined (_MESSAGING_SYNCSCOPEPOLICY__TRAITS_)
#define _MESSAGING_SYNCSCOPEPOLICY__TRAITS_
  
  template<>
  struct TAO_Messaging_Export Objref_Traits< ::Messaging::SyncScopePolicy>
  {
    static ::Messaging::SyncScopePolicy_ptr duplicate (
        ::Messaging::SyncScopePolicy_ptr p
      );
    static void release (
        ::Messaging::SyncScopePolicy_ptr p
      );
    static ::Messaging::SyncScopePolicy_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::Messaging::SyncScopePolicy_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#include /**/ "ace/post.h"

#endif /* ifndef */


