// -*- C++ -*-
//=============================================================================
/**
 * @file Auto_Functor.h
 *
 * $Id: Auto_Functor.h 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 * @deprecated This file is deprecated
 */
//=============================================================================
#ifndef TAO_UTILS_AUTO_FUNCTOR_H
#define TAO_UTILS_AUTO_FUNCTOR_H
#include /**/ "ace/pre.h"

#include "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

#include "ace/Auto_Functor.h"

namespace TAO
{
  namespace Utils
  {

  using namespace ACE_Utils;

  }
}

#include /**/ "ace/post.h"
#endif /* TAO_UTILS_AUTO_FUNCTOR_H */
