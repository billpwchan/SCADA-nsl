// -*- IDL -*-

/**
 * @file IORInterceptor.pidl
 *
 * $Id: IORInterceptor.pidl 935 2008-12-10 21:47:27Z mitza $
 *
 * @brief Pre-compiled IDL source for the IOR Interceptor
 * components in the ORB.
 *
 * The following is from orbos/99-12-02 Portable Interceptors spec,
 * the full IDL is downloadable from orbos/99-12-02. This file contains
 * interfaces from PortableInterceptor.idl that depend on
 * ObjectReferenceFactory and ObjectReferenceTemplate, which inherit
 * directly or indirectly from CORBA::ValueBase. This separate file
 * has been created to remove the dependency from the rest of module
 * PortableInterceptor.
 *
 * This file is used to generate the code in IORInterceptorC.*
 * The command used to generate code is:
 *
 *  tao_idl
 *     -o orig -Gp -Gd -Sa -St -Sci -SS
 *          -I$(TAO_ROOT)
 *          -Wb,export_macro=TAO_IORInterceptor_Export
 *          -Wb,export_include="iorinterceptor_export.h"
 *          -Wb,pre_include="ace/pre.h"
 *          -Wb,post_include="ace/post.h"
 *          IORInterceptor.pidl
 *
 * Please apply the patch in IORInterceptor.diff to the generated stubs and
 * skeletons.
 */

// File: IORInterceptor.idl
#ifndef _IOR_INTERCEPTOR_IDL_
#define _IOR_INTERCEPTOR_IDL_

#include "tao/PI/Interceptor.pidl"
#include "tao/IORInterceptor/IORInfo.pidl"
#include "tao/ObjRefTemplate/ObjectReferenceTemplate.pidl"

module PortableInterceptor
{
  typeprefix PortableInterceptor "omg.org";

  local interface IORInterceptor : Interceptor
  {
    void establish_components (in IORInfo info);
  };

  local interface IORInterceptor_3_0 : IORInterceptor
  {
    void components_established (in IORInfo info);

    void adapter_manager_state_changed (
        in AdapterManagerId id,
        in AdapterState state
      );
    void adapter_state_changed (
        in ObjectReferenceTemplateSeq templates,
        in AdapterState state
      );
  };
};

#endif  /* _IOR_INTERCEPTOR_IDL_ */
