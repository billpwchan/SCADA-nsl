// -*- C++ -*-
//
// $Id: Hashable.inl 14 2007-02-01 15:49:12Z mitza $


ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_INLINE
ACE_Hashable::ACE_Hashable (void)
  : hash_value_ (0)
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
