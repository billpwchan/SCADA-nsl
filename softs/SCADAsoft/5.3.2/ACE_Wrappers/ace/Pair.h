// -*- C++ -*-

//=============================================================================
/**
 *  @file    Pair.h
 *
 *  $Id: Pair.h 14 2007-02-01 15:49:12Z mitza $
 *
 *  ACE_Pair<> convenience header.
 *
 *  @author Irfan Pyarali
 */
//=============================================================================


#ifndef ACE_PAIR_H
#define ACE_PAIR_H

#include /**/ "ace/pre.h"

#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

// Include the templates here.
#include "ace/Pair_T.h"

#include /**/ "ace/post.h"

#endif /* ACE_PAIR_H */
