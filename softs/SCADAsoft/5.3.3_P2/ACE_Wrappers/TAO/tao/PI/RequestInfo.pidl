// -*- IDL -*-

/**
 * @file RequestInfo.pidl
 *
 * $Id: RequestInfo.pidl 935 2008-12-10 21:47:27Z mitza $
 *
 * @brief Pre-compiled RequestInfo
 *
 * This file is used to generate the code in RequestInfoC.*
 * The command used to generate code is:
 *
 *  tao_idl
 *     -o orig -Gp -Gd -GA -SS -Sci
 *          -Wb,export_include="tao/TAO_Export.h"
 *          -Wb,export_macro=TAO_Export
 *          -Wb,pre_include="ace/pre.h"
 *          -Wb,post_include="ace/post.h"
 *          RequestInfo.pidl
 */

#ifndef _REQUESTINFO_PIDL_
#define _REQUESTINFO_PIDL_

#include "tao/PI_Forward.pidl"
#include "tao/AnyTypeCode/Dynamic.pidl"
#include "tao/Messaging_SyncScope.pidl"
#include "tao/PI/InvalidSlot.pidl"
#include "tao/IOP.pidl"

module PortableInterceptor {

  typeprefix PortableInterceptor "omg.org";

  local interface RequestInfo
  {
    readonly attribute unsigned long request_id;
    readonly attribute string operation;
    readonly attribute Dynamic::ParameterList arguments;
    readonly attribute Dynamic::ExceptionList exceptions;
    readonly attribute Dynamic::ContextList contexts;
    readonly attribute Dynamic::RequestContext operation_context;
    readonly attribute any result;
    readonly attribute boolean response_expected;
    readonly attribute Messaging::SyncScope sync_scope;
    readonly attribute ReplyStatus reply_status;
    readonly attribute Object forward_reference;
    any get_slot (in SlotId id) raises (InvalidSlot);
    IOP::ServiceContext get_request_service_context (in IOP::ServiceId id);
    IOP::ServiceContext get_reply_service_context (in IOP::ServiceId id);
  };
};

#endif  /* _REQUESTINFO_PIDL_ */
