//
// $Id: be_visitor_union_fwd.cpp 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_union_fwd.cpp
//
// = DESCRIPTION
//    Visitors for generation of code for be_union_fwd
//
// = AUTHOR
//    Jeff Parsons
//
// ============================================================================

#include "be_union_fwd.h"
#include "be_union.h"

#include "be_visitor_union_fwd.h"
#include "be_visitor_context.h"
#include "be_helper.h"

#include "be_visitor_union_fwd/union_fwd_ch.cpp"

ACE_RCSID (be, 
           be_visitor_union_fwd, 
           "$Id: be_visitor_union_fwd.cpp 14 2007-02-01 15:49:12Z mitza $")
