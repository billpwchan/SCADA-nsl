//
// $Id: be_visitor_native.cpp 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_native.cpp
//
// = DESCRIPTION
//    Visitors for generation of code for native
//
// = AUTHOR
//    Johnny Willemsen
//
// ============================================================================

#include "be_native.h"
#include "be_helper.h"
#include "be_extern.h"

#include "be_visitor_native.h"
#include "be_visitor_context.h"

#include "be_visitor_native/native_ch.cpp"

ACE_RCSID (be,
           be_visitor_native,
           "$Id: be_visitor_native.cpp 14 2007-02-01 15:49:12Z mitza $")
