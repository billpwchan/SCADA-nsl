//
// $Id: be_visitor_field.cpp 14 2007-02-01 15:49:12Z mitza $
//
// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_field.cpp
//
// = DESCRIPTION
//    Concrete visitor for the base "BE_Field" node
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#include "be_type.h"
#include "be_field.h"
#include "be_array.h"
#include "be_typedef.h"
#include "be_enum.h"
#include "be_interface.h"
#include "be_interface_fwd.h"
#include "be_valuebox.h"
#include "be_valuetype.h"
#include "be_valuetype_fwd.h"
#include "be_component.h"
#include "be_component_fwd.h"
#include "be_eventtype.h"
#include "be_eventtype_fwd.h"
#include "be_predefined_type.h"
#include "be_sequence.h"
#include "be_string.h"
#include "be_structure.h"
#include "be_union.h"
#include "be_helper.h"
#include "utl_identifier.h"

#include "be_visitor_field.h"
#include "be_visitor_array.h"
#include "be_visitor_structure.h"
#include "be_visitor_union.h"
#include "be_visitor_context.h"
#include "ace/Log_Msg.h"

#include "be_visitor_field/field_ch.cpp"
#include "be_visitor_field/field_ci.cpp"
#include "be_visitor_field/field_cs.cpp"
#include "be_visitor_field/cdr_op_ch.cpp"
#include "be_visitor_field/cdr_op_cs.cpp"
#include "be_visitor_field/serializer_op_ch.cpp"
#include "be_visitor_field/serializer_op_cs.cpp"

ACE_RCSID (be, 
           be_visitor_field, 
           "$Id: be_visitor_field.cpp 14 2007-02-01 15:49:12Z mitza $")
