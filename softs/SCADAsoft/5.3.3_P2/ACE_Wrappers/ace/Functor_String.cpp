#include "ace/Functor_String.h"

#if !defined (__ACE_INLINE__)
#include "ace/Functor_String.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(ace, Functor, "$Id: Functor_String.cpp 14 2007-02-01 15:49:12Z mitza $")
