// $Id: OS_NS_sys_select.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_select.h"

ACE_RCSID(ace, OS_NS_sys_select, "$Id: OS_NS_sys_select.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_select.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

