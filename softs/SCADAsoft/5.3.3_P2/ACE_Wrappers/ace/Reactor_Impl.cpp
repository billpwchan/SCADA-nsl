// $Id: Reactor_Impl.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Reactor_Impl.h"

ACE_RCSID (ace,
           Reactor_Impl,
           "$Id: Reactor_Impl.cpp 14 2007-02-01 15:49:12Z mitza $")

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_Reactor_Impl::~ACE_Reactor_Impl (void)
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
