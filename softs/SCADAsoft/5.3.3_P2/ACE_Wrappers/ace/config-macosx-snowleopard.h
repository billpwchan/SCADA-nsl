/* -*- C++ -*- */
// $Id: config-macosx-snowleopard.h 1784 2011-01-19 22:12:23Z cleeland $

// This configuration file is designed to work with the Mac OSX 10.6.x, Snow Leopard

#ifndef ACE_CONFIG_MACOSX_SNOWLEOPARD_H
#define ACE_CONFIG_MACOSX_SNOWLEOPARD_H

#include "config-macosx-leopard.h"
#define ACE_LACKS_UCONTEXT_H // ucontext is deprecated in Snow Leopard

#endif /* ACE_CONFIG_MACOSX_SNOWLEOPARD_H */
