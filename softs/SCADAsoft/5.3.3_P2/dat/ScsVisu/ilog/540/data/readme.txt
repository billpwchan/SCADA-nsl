GZM-124
Those dbm files are copies of existing files in order to provide default message db when locale setting is not supported :
image.dbm, launcher.dbm and symbol.dbm are copies from $ROOTDIR\..\animator\source\anstudio\data\locale\en_US.US-ASCII
trdstudio.dbm is a copy from $ROOTDIR\dat\ScsVisu\scsstudio\trdstudio\locale\en_US.US-ASCII
dssstudio.dbm is a copy from $ROOTDIR\dat\ScsVisu\scsstudio\dssstudio\locale\en_US.US-ASCII
pnlstudio.dbm is a copy from $ROOTDIR\dat\ScsVisu\scsstudio\pnlstudio\locale\en_US.US-ASCII