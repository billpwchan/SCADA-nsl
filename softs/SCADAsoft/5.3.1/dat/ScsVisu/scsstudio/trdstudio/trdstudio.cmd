command QuitTrdStudio {
  acceleratorText "Ctrl+Q";
  acceleratorDefinition "<Ctrl><Key Q>";
  label   "&menu_scsquit";
  prompt  "&quitScsEditor";
  bitmap  "icquit.png";
  category studio;
  category scadasoft;
}
