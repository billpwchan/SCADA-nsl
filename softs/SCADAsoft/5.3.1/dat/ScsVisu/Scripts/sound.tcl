
###############################
#
# ----- Namespace
#
###############################

namespace eval Snd { }


###############################
#
# ---- Procedures
#
###############################


proc Snd::Scs_GetSoundVolume { } {
    Scs_GetSndVolume 
}

proc Snd::Scs_SetSoundVolume { Volume } {
    Scs_SetSndVolume $Volume
}

