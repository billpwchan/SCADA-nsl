//
//    ILOG Views Studio Command Description file 
//
//	Application Generation Extension
//

command GeneratePanel {
    interactive false;
    label "&generatePanel";
    bitmap "iccode";
}

command Generate {
    label "&generate";
    tooltip "&ttgenerate";
    prompt "&genCodePrompt";
    bitmap "iccode";
    category application;
}

command GenerateAll {
    label "&generateAll";
    prompt "&genAllPrompt";
    category application;
}

command GenerateApplication {
    label "&genAppLabel";
    prompt "&generateAppPrompt";
    category application;
}

command GeneratePanelClass {
    label "&genPanClass";
    tooltip "&ttgenPanClass";
    bitmap "iccode";
    prompt "&genPanClassPrompt";
    category application;
}

command GeneratePanelSubClass {
    label  "&genPanSubClass";
    prompt "&genPanSubClassP";
    category application;
}

command GenerateMakeFile {
    label "&genMakeFile";
    prompt "&genMakeFilePrompt";
    category application;
}


