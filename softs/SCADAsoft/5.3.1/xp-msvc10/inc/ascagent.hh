// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_IDL_G__BIN_P530_SRC_ASC2_IDL_ASCAGENT_H_
#define _TAO_IDL_G__BIN_P530_SRC_ASC2_IDL_ASCAGENT_H_


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/ORB.h"
#include "tao/SystemException.h"
#include "tao/Basic_Types.h"
#include "tao/ORB_Constants.h"
#include "tao/Object.h"
#include "tao/String_Manager_T.h"
#include "tao/Sequence_T.h"
#include "tao/Objref_VarOut_T.h"
#include "tao/Seq_Var_T.h"
#include "tao/Seq_Out_T.h"
#include "tao/VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#include "scsprocess.hh"
#include "scsredtype.hh"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO 

// TAO_IDL - Generated from 
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root_ch.cpp:62
TAO_BEGIN_VERSIONED_NAMESPACE_DECL



namespace TAO
{
  class Collocation_Proxy_Broker;
  template<typename T> class Narrow_Utils;
}
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:413

typedef char * ScsObjectIdl;
typedef ::CORBA::String_var ScsObjectIdl_var;
typedef ::CORBA::String_out ScsObjectIdl_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::LongSeq LongList;
typedef CORBA::LongSeq_var LongList_var;
typedef CORBA::LongSeq_out LongList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::ULongSeq ULongList;
typedef CORBA::ULongSeq_var ULongList_var;
typedef CORBA::ULongSeq_out ULongList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::LongLongSeq LongLongList;
typedef CORBA::LongLongSeq_var LongLongList_var;
typedef CORBA::LongLongSeq_out LongLongList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::ULongLongSeq ULongLongList;
typedef CORBA::ULongLongSeq_var ULongLongList_var;
typedef CORBA::ULongLongSeq_out ULongLongList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::FloatSeq FloatList;
typedef CORBA::FloatSeq_var FloatList_var;
typedef CORBA::FloatSeq_out FloatList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::DoubleSeq DoubleList;
typedef CORBA::DoubleSeq_var DoubleList_var;
typedef CORBA::DoubleSeq_out DoubleList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::StringSeq StringList;
typedef CORBA::StringSeq_var StringList_var;
typedef CORBA::StringSeq_out StringList_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:472

typedef CORBA::OctetSeq OctetList;
typedef CORBA::OctetSeq_var OctetList_var;
typedef CORBA::OctetSeq_out OctetList_out;

// TAO_IDL - Generated from
// be\be_type.cpp:269

struct ScsProcessInfoIdl;

typedef
  TAO_Var_Var_T<
      ScsProcessInfoIdl
    >
  ScsProcessInfoIdl_var;

typedef
  TAO_Out_T<
      ScsProcessInfoIdl
    >
  ScsProcessInfoIdl_out;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57

struct  ScsProcessInfoIdl
{
  typedef ScsProcessInfoIdl_var _var_type;
  typedef ScsProcessInfoIdl_out _out_type;
  
  static void _tao_any_destructor (void *);
  TAO::String_Manager name;
  TAO::String_Manager state;
  ::CORBA::Short phase;
  ::CORBA::Char terminate;
  ::CORBA::Char notification;
  ::CORBA::Short shutdownOrder;
  ::CORBA::Char supervisionType;
  ::CORBA::Short priority;
  TAO::String_Manager runString;
  ::CORBA::Char snapshot;
  ::CORBA::ULong initTimeout;
  ::CORBA::ULong pid;
};

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49

extern  ::CORBA::TypeCode_ptr const _tc_ScsProcessInfoIdl;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_SCSPROCESSINFOSIDL_CH_)
#define _SCSPROCESSINFOSIDL_CH_

class ScsProcessInfosIdl;

typedef
  TAO_VarSeq_Var_T<
      ScsProcessInfosIdl
    >
  ScsProcessInfosIdl_var;

typedef
  TAO_Seq_Out_T<
      ScsProcessInfosIdl
    >
  ScsProcessInfosIdl_out;

class  ScsProcessInfosIdl
  : public
      TAO::unbounded_value_sequence<
          ScsProcessInfoIdl
        >
{
public:
  ScsProcessInfosIdl (void);
  ScsProcessInfosIdl ( ::CORBA::ULong max);
  ScsProcessInfosIdl (
      ::CORBA::ULong max,
      ::CORBA::ULong length,
      ScsProcessInfoIdl* buffer, 
      ::CORBA::Boolean release = false
    );
  ScsProcessInfosIdl (const ScsProcessInfosIdl &);
  virtual ~ScsProcessInfosIdl (void);
  
  static void _tao_any_destructor (void *);
  
  typedef ScsProcessInfosIdl_var _var_type;
  typedef ScsProcessInfosIdl_out _out_type;
  
  
};

#endif /* end #if !defined */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49

extern  ::CORBA::TypeCode_ptr const _tc_ScsProcessInfosIdl;

// TAO_IDL - Generated from
// be\be_interface.cpp:644

#if !defined (_ASCIDL__VAR_OUT_CH_)
#define _ASCIDL__VAR_OUT_CH_

class AscIdl;
typedef AscIdl *AscIdl_ptr;

typedef
  TAO_Objref_Var_T<
      AscIdl
    >
  AscIdl_var;

typedef
  TAO_Objref_Out_T<
      AscIdl
    >
  AscIdl_out;

#endif /* end #if !defined */

// TAO_IDL - Generated from
// be\be_interface.cpp:644

#if !defined (_ASCAGENTIDL__VAR_OUT_CH_)
#define _ASCAGENTIDL__VAR_OUT_CH_

class AscAgentIdl;
typedef AscAgentIdl *AscAgentIdl_ptr;

typedef
  TAO_Objref_Var_T<
      AscAgentIdl
    >
  AscAgentIdl_var;

typedef
  TAO_Objref_Out_T<
      AscAgentIdl
    >
  AscAgentIdl_out;

#endif /* end #if !defined */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:54

#if !defined (_ASCAGENTIDL_CH_)
#define _ASCAGENTIDL_CH_

class  AscAgentIdl
  : public virtual ::ScsProcessIdl
{
public:
  friend class TAO::Narrow_Utils<AscAgentIdl>;
  typedef AscAgentIdl_ptr _ptr_type;
  typedef AscAgentIdl_var _var_type;
  typedef AscAgentIdl_out _out_type;
  
  // The static operations.
  static AscAgentIdl_ptr _duplicate (AscAgentIdl_ptr obj);
  
  static void _tao_release (AscAgentIdl_ptr obj);
  
  static AscAgentIdl_ptr _narrow (::CORBA::Object_ptr obj);
  static AscAgentIdl_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
  static AscAgentIdl_ptr _nil (void)
  {
    return static_cast<AscAgentIdl_ptr> (0);
  }
  
  static void _tao_any_destructor (void *);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl subscribe (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getState (
      const char * procName,
      ::CORBA::String_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getRemoteState (
      const char * procName,
      ::CORBA::String_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl setState (
      const char * procName,
      const char * state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl specifyState (
      const char * procName,
      const char * state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl startProcessInfo (
      ::CORBA::String_out processName,
      ::CORBA::Short_out phase,
      ::CORBA::Short_out nbprocesstostart);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl runCmd (
      const char * serverName,
      const char * terminate,
      const char * notification,
      const char * supervisionType,
      const char * priority,
      const char * snapshot,
      const char * command);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl runCommand (
      const char * serverName,
      const char * command);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl synchroniseStandby (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl switchover (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getSnapshot (
      ::AscAgentIdl_ptr standby);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl forceSnapshot (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl forceSnapshotIn (
      const char * path);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl forceSnapshotWithStatus (
      const char * path,
      ::AscIdl_ptr fromServer);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual void snapshotCompleted (
      const char * remoteSnapShotPath);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual void endSnapshotCompleted (
      ::CORBA::Boolean successful);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl foregroundSnapshot (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl endBackgroundSnapshot (
      const ::ScsStatusIdl & snapshotStatus);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual void foregroundSnapshotReply (
      const ::ScsMessageTag & ascAgentTag,
      const ::ScsStatusIdl & result);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl testWatchdog (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getWatchdogLastLifeTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getWatchdogMode (
      ::CORBA::Short_out mode);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getNet1SendState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastNet1SendStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getNet1RecvState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastNet1RecvStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getNet2SendState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastNet2SendStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getNet2RecvState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastNet2RecvStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getSerSendState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastSerSendStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getSerRecvState (
      ::CORBA::Short_out state);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getLastSerRecvStateTime (
      ::ScsTimeValIdl_out lasttime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getOSTime (
      ::ScsTimeValIdl_out ostime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getRemoteOSTime (
      ::ScsTimeValIdl_out ostime);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getProcessInfos (
      ::ScsProcessInfosIdl & infos,
      ::CORBA::Short_out currentPhase);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl getProcessArguments (
      const char * procName,
      ::StringList_out args);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl refreshEnvList (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl authorizedExternalClientConnection (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
  
  virtual ::ScsStatusIdl Shutdown (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:216
  
  virtual ::CORBA::Boolean _is_a (const char *type_id);
  virtual const char* _interface_repository_id (void) const;
  virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
private:
  TAO::Collocation_Proxy_Broker *the_TAO_AscAgentIdl_Proxy_Broker_;

protected:
  // Concrete interface only.
  AscAgentIdl (void);
  
  // These methods travese the inheritance tree and set the
  // parents piece of the given class in the right mode.
  virtual void AscAgentIdl_setup_collocation (void);
  
  // Concrete non-local interface only.
  AscAgentIdl (
      ::IOP::IOR *ior,
      TAO_ORB_Core *orb_core);
  
  // Non-local interface only.
  AscAgentIdl (
      TAO_Stub *objref,
      ::CORBA::Boolean _tao_collocated = false,
      TAO_Abstract_ServantBase *servant = 0,
      TAO_ORB_Core *orb_core = 0);
  
  virtual ~AscAgentIdl (void);

private:
  // Private and unimplemented for concrete interfaces.
  AscAgentIdl (const AscAgentIdl &);
  
  void operator= (const AscAgentIdl &);
};

#endif /* end #if !defined */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49

extern  ::CORBA::TypeCode_ptr const _tc_AscAgentIdl;

// Proxy Broker Factory function pointer declarations.

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root.cpp:139

extern 
TAO::Collocation_Proxy_Broker *
(*_TAO_AscAgentIdl_Proxy_Broker_Factory_function_pointer) (
    ::CORBA::Object_ptr obj
  );

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{

#if !defined (_ASCIDL__TRAITS_)
#define _ASCIDL__TRAITS_
  
  template<>
  struct  Objref_Traits< ::AscIdl>
  {
    static ::AscIdl_ptr duplicate (
        ::AscIdl_ptr p
      );
    static void release (
        ::AscIdl_ptr p
      );
    static ::AscIdl_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::AscIdl_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */

#if !defined (_ASCAGENTIDL__TRAITS_)
#define _ASCAGENTIDL__TRAITS_
  
  template<>
  struct  Objref_Traits< ::AscAgentIdl>
  {
    static ::AscAgentIdl_ptr duplicate (
        ::AscAgentIdl_ptr p
      );
    static void release (
        ::AscAgentIdl_ptr p
      );
    static ::AscAgentIdl_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::AscAgentIdl_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */
}
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= (::CORBA::Any &, const ScsProcessInfoIdl &); // copying version
 void operator<<= (::CORBA::Any &, ScsProcessInfoIdl*); // noncopying version
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, ScsProcessInfoIdl *&); // deprecated
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const ScsProcessInfoIdl *&);
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= ( ::CORBA::Any &, const ScsProcessInfosIdl &); // copying version
 void operator<<= ( ::CORBA::Any &, ScsProcessInfosIdl*); // noncopying version
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, ScsProcessInfosIdl *&); // deprecated
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const ScsProcessInfosIdl *&);
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface_fwd/any_op_ch.cpp:65


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= (::CORBA::Any &, AscIdl_ptr); // copying
 void operator<<= (::CORBA::Any &, AscIdl_ptr *); // non-copying
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, AscIdl *&);
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/any_op_ch.cpp:54


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= (::CORBA::Any &, AscAgentIdl_ptr); // copying
 void operator<<= (::CORBA::Any &, AscAgentIdl_ptr *); // non-copying
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, AscAgentIdl_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 ::CORBA::Boolean operator<< (TAO_OutputCDR &, const ScsProcessInfoIdl &);
 ::CORBA::Boolean operator>> (TAO_InputCDR &, ScsProcessInfoIdl &);

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_ScsProcessInfosIdl_H_
#define _TAO_CDR_OP_ScsProcessInfosIdl_H_
TAO_BEGIN_VERSIONED_NAMESPACE_DECL



 ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const ScsProcessInfosIdl &_tao_sequence
  );
 ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    ScsProcessInfosIdl &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL



#endif /* _TAO_CDR_OP_ScsProcessInfosIdl_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface_fwd/cdr_op_ch.cpp:64


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 ::CORBA::Boolean operator<< (TAO_OutputCDR &, const AscIdl_ptr );
 ::CORBA::Boolean operator>> (TAO_InputCDR &, AscIdl_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/cdr_op_ch.cpp:55

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 ::CORBA::Boolean operator<< (TAO_OutputCDR &, const AscAgentIdl_ptr );
 ::CORBA::Boolean operator>> (TAO_InputCDR &, AscAgentIdl_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be\be_codegen.cpp:1228
#if defined (__ACE_INLINE__)
#include "ascagent.inl"
#endif /* defined INLINE */

#endif /* ifndef */


