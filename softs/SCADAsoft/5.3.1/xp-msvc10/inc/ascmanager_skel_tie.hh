// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:645

#ifndef _TAO_IDL_G__BIN_P530_SRC_ASC2_IDL_ASCMANAGER_SKEL_TIE_H_
#define _TAO_IDL_G__BIN_P530_SRC_ASC2_IDL_ASCMANAGER_SKEL_TIE_H_



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/tie_sh.cpp:87

// TIE class: Refer to CORBA v2.2, Section 20.34.4
template <class T>
class  POA_AscManagerIdl_tie : public POA_AscManagerIdl
{
public:
  /// the T& ctor
  POA_AscManagerIdl_tie (T &t);
  /// ctor taking a POA
  POA_AscManagerIdl_tie (T &t, PortableServer::POA_ptr poa);
  /// ctor taking pointer and an ownership flag
  POA_AscManagerIdl_tie (T *tp, ::CORBA::Boolean release = true);
  /// ctor with T*, ownership flag and a POA
  POA_AscManagerIdl_tie (
      T *tp,
      PortableServer::POA_ptr poa,
      ::CORBA::Boolean release = true
    );
  /// dtor
  
  ~POA_AscManagerIdl_tie (void);
  // TIE specific functions
  /// return the underlying object
  T *_tied_object (void);
  /// set the underlying object
  void _tied_object (T &obj);
  /// set the underlying object and the ownership flag
  void _tied_object (T *obj, ::CORBA::Boolean release = true);
  /// do we own it
  ::CORBA::Boolean _is_owner (void);
  /// set the ownership
  
  void _is_owner ( ::CORBA::Boolean b);
  // overridden ServantBase operations
  PortableServer::POA_ptr _default_POA (void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  char * scs_implementation (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  char * scs_host (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  void isAlive (
      void);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getMode (
      ::ScsTimeValIdl_out,
      ::CORBA::String_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getState (
      const char *,
      const char *,
      ::CORBA::String_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl setState (
      const char *,
      const char *,
      const char *);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getPhysState (
      const char *,
      const char *,
      ::CORBA::String_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl subscribe (
      const char *);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getColumnName (
      const char *,
      ::CORBA::String_out,
      ::CORBA::Short_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl unsubscribe (
      const char *);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getAllStates (
      const char *,
      ::CORBA::String_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getHosts (
      const char *,
      ::StringList_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getEnvs (
      const char *,
      ::StringList_out);
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/tie_sh.cpp:60
  
  ::ScsStatusIdl getRedundancyType (
      const char *,
      ::CORBA::UShort_out);

private:
  T *ptr_;
  PortableServer::POA_var poa_;
  ::CORBA::Boolean rel_;
  
  // copy and assignment are not allowed
  POA_AscManagerIdl_tie (const POA_AscManagerIdl_tie &);
  void operator= (const POA_AscManagerIdl_tie &);
};

// TAO_IDL - Generated from 
// be\be_codegen.cpp:1391


#if defined (ACE_TEMPLATES_REQUIRE_SOURCE)
#include "ascmanager_skel_tie.cpp"
#endif /* defined REQUIRED SOURCE */

#if defined (ACE_TEMPLATES_REQUIRE_PRAGMA)
#pragma implementation ("ascmanager_skel_tie.cpp")
#endif /* defined REQUIRED PRAGMA */

#endif /* ifndef */

