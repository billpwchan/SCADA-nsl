// $Id: Shared_Memory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Shared_Memory.h"

ACE_RCSID(ace, Shared_Memory, "$Id: Shared_Memory.cpp 14 2007-02-01 15:49:12Z mitza $")

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_Shared_Memory::~ACE_Shared_Memory (void)
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
