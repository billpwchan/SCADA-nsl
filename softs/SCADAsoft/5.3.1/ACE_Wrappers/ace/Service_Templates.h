
//=============================================================================
/**
 *  @file   Service_Templates.h
 *
 *  $Id: Service_Templates.h 14 2007-02-01 15:49:12Z mitza $
 *
 *  @author Priyanka Gontla <pgontla@ece.uci.edu>
 */
//=============================================================================


#ifndef ACE_SERVICE_TEMPLATES_H
#define ACE_SERVICE_TEMPLATES_H
#include /**/ "ace/pre.h"

#include "ace/Svc_Conf.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

#include "ace/Auto_Ptr.h"
#include "ace/Thread_Manager.h"
#include "ace/Stream_Modules.h"
#include "ace/Stream.h"

#include /**/ "ace/post.h"
#endif /* ACE_SERVICE_TEMPLATES_H */
