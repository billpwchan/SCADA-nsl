// $Id: OS_NS_sys_mman.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_mman.h"

ACE_RCSID(ace, OS_NS_sys_mman, "$Id: OS_NS_sys_mman.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_mman.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

