// $Id: Containers.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Containers.h"

ACE_RCSID (ace,
           Containers,
           "$Id: Containers.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
#include "ace/Containers.inl"
#endif /* __ACE_INLINE__ */

