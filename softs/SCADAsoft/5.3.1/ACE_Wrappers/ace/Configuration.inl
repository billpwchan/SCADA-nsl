// -*- C++ -*-
//
// $Id: Configuration.inl 978 2008-12-31 20:17:52Z mitza $

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_INLINE const ACE_TCHAR*
ACE_Configuration_ExtId::name (void)
{
  return name_;
}

ACE_END_VERSIONED_NAMESPACE_DECL
