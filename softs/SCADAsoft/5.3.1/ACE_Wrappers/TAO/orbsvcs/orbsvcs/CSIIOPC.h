// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_IDL_CSIIOPC_H_
#define _TAO_IDL_CSIIOPC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "orbsvcs/Security/security_export.h"
#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/ORB.h"
#include "tao/Basic_Types.h"
#include "tao/String_Manager_T.h"
#include "tao/AnyTypeCode/IOPA.h"
#include "tao/Sequence_T.h"
#include "tao/Seq_Var_T.h"
#include "tao/Seq_Out_T.h"
#include "tao/VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#include "tao/IOPC.h"
#include "CSIC.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO TAO_Security_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace IOP
{
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_CSI_SEC_MECH_LIST = 33U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_NULL_TAG = 34U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_SECIOP_SEC_TRANS = 35U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_TLS_SEC_TRANS = 36U;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module IOP

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace CSIIOP
{
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
  
  typedef ::CORBA::UShort AssociationOptions;
  typedef ::CORBA::UShort_out AssociationOptions_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_AssociationOptions;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions NoProtection = 1U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions Integrity = 2U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions Confidentiality = 4U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions DetectReplay = 8U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions DetectMisordering = 16U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions EstablishTrustInTarget = 32U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions EstablishTrustInClient = 64U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions NoDelegation = 128U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions SimpleDelegation = 256U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions CompositeDelegation = 512U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions IdentityAssertion = 1024U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::AssociationOptions DelegationByClient = 2048U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
  
  typedef ::CORBA::ULong ServiceConfigurationSyntax;
  typedef ::CORBA::ULong_out ServiceConfigurationSyntax_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_ServiceConfigurationSyntax;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::ServiceConfigurationSyntax SCS_GeneralNames = 324816U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const CSIIOP::ServiceConfigurationSyntax SCS_GSSExportedName = 324817U;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_CSIIOP_SERVICESPECIFICNAME_CH_)
#define _CSIIOP_SERVICESPECIFICNAME_CH_
  
  class ServiceSpecificName;
  
  typedef
    TAO_FixedSeq_Var_T<
        ServiceSpecificName
      >
    ServiceSpecificName_var;
  
  typedef
    TAO_Seq_Out_T<
        ServiceSpecificName
      >
    ServiceSpecificName_out;
  
  class TAO_Security_Export ServiceSpecificName
    : public
        TAO::unbounded_value_sequence<
            ::CORBA::Octet
          >
  {
  public:
    ServiceSpecificName (void);
    ServiceSpecificName ( ::CORBA::ULong max);
    ServiceSpecificName (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        ::CORBA::Octet* buffer, 
        ::CORBA::Boolean release = false
      );
    ServiceSpecificName (const ServiceSpecificName &);
    virtual ~ServiceSpecificName (void);
    
    static void _tao_any_destructor (void *);
    
    typedef ServiceSpecificName_var _var_type;
    typedef ServiceSpecificName_out _out_type;
    
    

#if (TAO_NO_COPY_OCTET_SEQUENCES == 1)
    ServiceSpecificName (
        ::CORBA::ULong length,
        const ACE_Message_Block* mb
      )
      : TAO::unbounded_value_sequence< ::CORBA::Octet> (length, mb) {}
#endif /* TAO_NO_COPY_OCTET_SEQUENCE == 1 */
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_ServiceSpecificName;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct ServiceConfiguration;
  
  typedef
    TAO_Var_Var_T<
        ServiceConfiguration
      >
    ServiceConfiguration_var;
  
  typedef
    TAO_Out_T<
        ServiceConfiguration
      >
    ServiceConfiguration_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export ServiceConfiguration
  {
    typedef ServiceConfiguration_var _var_type;
    typedef ServiceConfiguration_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::ServiceConfigurationSyntax syntax;
    CSIIOP::ServiceSpecificName name;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_ServiceConfiguration;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_CSIIOP_SERVICECONFIGURATIONLIST_CH_)
#define _CSIIOP_SERVICECONFIGURATIONLIST_CH_
  
  class ServiceConfigurationList;
  
  typedef
    TAO_VarSeq_Var_T<
        ServiceConfigurationList
      >
    ServiceConfigurationList_var;
  
  typedef
    TAO_Seq_Out_T<
        ServiceConfigurationList
      >
    ServiceConfigurationList_out;
  
  class TAO_Security_Export ServiceConfigurationList
    : public
        TAO::unbounded_value_sequence<
            ServiceConfiguration
          >
  {
  public:
    ServiceConfigurationList (void);
    ServiceConfigurationList ( ::CORBA::ULong max);
    ServiceConfigurationList (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        ServiceConfiguration* buffer, 
        ::CORBA::Boolean release = false
      );
    ServiceConfigurationList (const ServiceConfigurationList &);
    virtual ~ServiceConfigurationList (void);
    
    static void _tao_any_destructor (void *);
    
    typedef ServiceConfigurationList_var _var_type;
    typedef ServiceConfigurationList_out _out_type;
    
    
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_ServiceConfigurationList;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct AS_ContextSec;
  
  typedef
    TAO_Var_Var_T<
        AS_ContextSec
      >
    AS_ContextSec_var;
  
  typedef
    TAO_Out_T<
        AS_ContextSec
      >
    AS_ContextSec_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export AS_ContextSec
  {
    typedef AS_ContextSec_var _var_type;
    typedef AS_ContextSec_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::AssociationOptions target_supports;
    CSIIOP::AssociationOptions target_requires;
    CSI::OID client_authentication_mech;
    CSI::GSS_NT_ExportedName target_name;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_AS_ContextSec;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct SAS_ContextSec;
  
  typedef
    TAO_Var_Var_T<
        SAS_ContextSec
      >
    SAS_ContextSec_var;
  
  typedef
    TAO_Out_T<
        SAS_ContextSec
      >
    SAS_ContextSec_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export SAS_ContextSec
  {
    typedef SAS_ContextSec_var _var_type;
    typedef SAS_ContextSec_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::AssociationOptions target_supports;
    CSIIOP::AssociationOptions target_requires;
    CSIIOP::ServiceConfigurationList privilege_authorities;
    CSI::OIDList supported_naming_mechanisms;
    CSI::IdentityTokenType supported_identity_types;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_SAS_ContextSec;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct CompoundSecMech;
  
  typedef
    TAO_Var_Var_T<
        CompoundSecMech
      >
    CompoundSecMech_var;
  
  typedef
    TAO_Out_T<
        CompoundSecMech
      >
    CompoundSecMech_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export CompoundSecMech
  {
    typedef CompoundSecMech_var _var_type;
    typedef CompoundSecMech_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::AssociationOptions target_requires;
    IOP::TaggedComponent transport_mech;
    CSIIOP::AS_ContextSec as_context_mech;
    CSIIOP::SAS_ContextSec sas_context_mech;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_CompoundSecMech;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_CSIIOP_COMPOUNDSECMECHANISMS_CH_)
#define _CSIIOP_COMPOUNDSECMECHANISMS_CH_
  
  class CompoundSecMechanisms;
  
  typedef
    TAO_VarSeq_Var_T<
        CompoundSecMechanisms
      >
    CompoundSecMechanisms_var;
  
  typedef
    TAO_Seq_Out_T<
        CompoundSecMechanisms
      >
    CompoundSecMechanisms_out;
  
  class TAO_Security_Export CompoundSecMechanisms
    : public
        TAO::unbounded_value_sequence<
            CompoundSecMech
          >
  {
  public:
    CompoundSecMechanisms (void);
    CompoundSecMechanisms ( ::CORBA::ULong max);
    CompoundSecMechanisms (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        CompoundSecMech* buffer, 
        ::CORBA::Boolean release = false
      );
    CompoundSecMechanisms (const CompoundSecMechanisms &);
    virtual ~CompoundSecMechanisms (void);
    
    static void _tao_any_destructor (void *);
    
    typedef CompoundSecMechanisms_var _var_type;
    typedef CompoundSecMechanisms_out _out_type;
    
    
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_CompoundSecMechanisms;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct CompoundSecMechList;
  
  typedef
    TAO_Var_Var_T<
        CompoundSecMechList
      >
    CompoundSecMechList_var;
  
  typedef
    TAO_Out_T<
        CompoundSecMechList
      >
    CompoundSecMechList_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export CompoundSecMechList
  {
    typedef CompoundSecMechList_var _var_type;
    typedef CompoundSecMechList_out _out_type;
    
    static void _tao_any_destructor (void *);
    ::CORBA::Boolean stateful;
    CSIIOP::CompoundSecMechanisms mechanism_list;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_CompoundSecMechList;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct TransportAddress;
  
  typedef
    TAO_Var_Var_T<
        TransportAddress
      >
    TransportAddress_var;
  
  typedef
    TAO_Out_T<
        TransportAddress
      >
    TransportAddress_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export TransportAddress
  {
    typedef TransportAddress_var _var_type;
    typedef TransportAddress_out _out_type;
    
    static void _tao_any_destructor (void *);
    TAO::String_Manager host_name;
    ::CORBA::UShort port;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_TransportAddress;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_CSIIOP_TRANSPORTADDRESSLIST_CH_)
#define _CSIIOP_TRANSPORTADDRESSLIST_CH_
  
  class TransportAddressList;
  
  typedef
    TAO_VarSeq_Var_T<
        TransportAddressList
      >
    TransportAddressList_var;
  
  typedef
    TAO_Seq_Out_T<
        TransportAddressList
      >
    TransportAddressList_out;
  
  class TAO_Security_Export TransportAddressList
    : public
        TAO::unbounded_value_sequence<
            TransportAddress
          >
  {
  public:
    TransportAddressList (void);
    TransportAddressList ( ::CORBA::ULong max);
    TransportAddressList (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        TransportAddress* buffer, 
        ::CORBA::Boolean release = false
      );
    TransportAddressList (const TransportAddressList &);
    virtual ~TransportAddressList (void);
    
    static void _tao_any_destructor (void *);
    
    typedef TransportAddressList_var _var_type;
    typedef TransportAddressList_out _out_type;
    
    
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_TransportAddressList;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_SECIOP_SEC_TRANS = 35U;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct SECIOP_SEC_TRANS;
  
  typedef
    TAO_Var_Var_T<
        SECIOP_SEC_TRANS
      >
    SECIOP_SEC_TRANS_var;
  
  typedef
    TAO_Out_T<
        SECIOP_SEC_TRANS
      >
    SECIOP_SEC_TRANS_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export SECIOP_SEC_TRANS
  {
    typedef SECIOP_SEC_TRANS_var _var_type;
    typedef SECIOP_SEC_TRANS_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::AssociationOptions target_supports;
    CSIIOP::AssociationOptions target_requires;
    CSI::OID mech_oid;
    CSI::GSS_NT_ExportedName target_name;
    CSIIOP::TransportAddressList addresses;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_SECIOP_SEC_TRANS;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_constant/constant_ch.cpp:52
  
  const IOP::ComponentId TAG_TLS_SEC_TRANS = 36U;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct TLS_SEC_TRANS;
  
  typedef
    TAO_Var_Var_T<
        TLS_SEC_TRANS
      >
    TLS_SEC_TRANS_var;
  
  typedef
    TAO_Out_T<
        TLS_SEC_TRANS
      >
    TLS_SEC_TRANS_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct TAO_Security_Export TLS_SEC_TRANS
  {
    typedef TLS_SEC_TRANS_var _var_type;
    typedef TLS_SEC_TRANS_out _out_type;
    
    static void _tao_any_destructor (void *);
    CSIIOP::AssociationOptions target_supports;
    CSIIOP::AssociationOptions target_requires;
    CSIIOP::TransportAddressList addresses;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_Security_Export ::CORBA::TypeCode_ptr const _tc_TLS_SEC_TRANS;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module CSIIOP

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= ( ::CORBA::Any &, const CSIIOP::ServiceSpecificName &); // copying version
TAO_Security_Export void operator<<= ( ::CORBA::Any &, CSIIOP::ServiceSpecificName*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::ServiceSpecificName *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::ServiceSpecificName *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::ServiceConfiguration &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::ServiceConfiguration*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::ServiceConfiguration *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::ServiceConfiguration *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= ( ::CORBA::Any &, const CSIIOP::ServiceConfigurationList &); // copying version
TAO_Security_Export void operator<<= ( ::CORBA::Any &, CSIIOP::ServiceConfigurationList*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::ServiceConfigurationList *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::ServiceConfigurationList *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::AS_ContextSec &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::AS_ContextSec*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::AS_ContextSec *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::AS_ContextSec *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::SAS_ContextSec &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::SAS_ContextSec*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::SAS_ContextSec *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::SAS_ContextSec *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::CompoundSecMech &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::CompoundSecMech*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::CompoundSecMech *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::CompoundSecMech *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= ( ::CORBA::Any &, const CSIIOP::CompoundSecMechanisms &); // copying version
TAO_Security_Export void operator<<= ( ::CORBA::Any &, CSIIOP::CompoundSecMechanisms*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::CompoundSecMechanisms *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::CompoundSecMechanisms *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::CompoundSecMechList &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::CompoundSecMechList*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::CompoundSecMechList *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::CompoundSecMechList *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::TransportAddress &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::TransportAddress*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::TransportAddress *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::TransportAddress *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= ( ::CORBA::Any &, const CSIIOP::TransportAddressList &); // copying version
TAO_Security_Export void operator<<= ( ::CORBA::Any &, CSIIOP::TransportAddressList*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::TransportAddressList *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::TransportAddressList *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::SECIOP_SEC_TRANS &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::SECIOP_SEC_TRANS*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::SECIOP_SEC_TRANS *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::SECIOP_SEC_TRANS *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export void operator<<= (::CORBA::Any &, const CSIIOP::TLS_SEC_TRANS &); // copying version
TAO_Security_Export void operator<<= (::CORBA::Any &, CSIIOP::TLS_SEC_TRANS*); // noncopying version
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, CSIIOP::TLS_SEC_TRANS *&); // deprecated
TAO_Security_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const CSIIOP::TLS_SEC_TRANS *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_CSIIOP_ServiceSpecificName_H_
#define _TAO_CDR_OP_CSIIOP_ServiceSpecificName_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_Security_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const CSIIOP::ServiceSpecificName &_tao_sequence
  );
TAO_Security_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    CSIIOP::ServiceSpecificName &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_CSIIOP_ServiceSpecificName_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::ServiceConfiguration &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::ServiceConfiguration &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_CSIIOP_ServiceConfigurationList_H_
#define _TAO_CDR_OP_CSIIOP_ServiceConfigurationList_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_Security_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const CSIIOP::ServiceConfigurationList &_tao_sequence
  );
TAO_Security_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    CSIIOP::ServiceConfigurationList &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_CSIIOP_ServiceConfigurationList_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::AS_ContextSec &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::AS_ContextSec &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::SAS_ContextSec &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::SAS_ContextSec &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::CompoundSecMech &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::CompoundSecMech &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_CSIIOP_CompoundSecMechanisms_H_
#define _TAO_CDR_OP_CSIIOP_CompoundSecMechanisms_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_Security_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const CSIIOP::CompoundSecMechanisms &_tao_sequence
  );
TAO_Security_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    CSIIOP::CompoundSecMechanisms &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_CSIIOP_CompoundSecMechanisms_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::CompoundSecMechList &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::CompoundSecMechList &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::TransportAddress &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::TransportAddress &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_CSIIOP_TransportAddressList_H_
#define _TAO_CDR_OP_CSIIOP_TransportAddressList_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_Security_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const CSIIOP::TransportAddressList &_tao_sequence
  );
TAO_Security_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    CSIIOP::TransportAddressList &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_CSIIOP_TransportAddressList_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::SECIOP_SEC_TRANS &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::SECIOP_SEC_TRANS &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Security_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const CSIIOP::TLS_SEC_TRANS &);
TAO_Security_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, CSIIOP::TLS_SEC_TRANS &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#if defined (__ACE_INLINE__)
#include "CSIIOPC.inl"
#endif /* defined INLINE */

#include /**/ "ace/post.h"

#endif /* ifndef */


