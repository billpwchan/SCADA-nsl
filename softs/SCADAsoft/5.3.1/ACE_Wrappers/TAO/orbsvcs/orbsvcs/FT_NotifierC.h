// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_IDL_FT_NOTIFIERC_H_
#define _TAO_IDL_FT_NOTIFIERC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "orbsvcs/FaultTolerance/fault_tol_export.h"
#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/ORB.h"
#include "tao/SystemException.h"
#include "tao/Basic_Types.h"
#include "tao/ORB_Constants.h"
#include "tao/Object.h"
#include "tao/String_Manager_T.h"
#include "tao/Objref_VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#include "orbsvcs/FT_CORBA_ORBC.h"
#include "orbsvcs/FT_ReplicaC.h"
#include "orbsvcs/CosNamingC.h"
#include "orbsvcs/CosNotificationC.h"
#include "orbsvcs/CosNotifyFilterC.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO TAO_FT_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from 
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root_ch.cpp:62

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



namespace TAO
{
  class Collocation_Proxy_Broker;
  template<typename T> class Narrow_Utils;
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace FT
{
  
  // TAO_IDL - Generated from
  // be\be_interface.cpp:644

#if !defined (_FT_FAULTNOTIFIER__VAR_OUT_CH_)
#define _FT_FAULTNOTIFIER__VAR_OUT_CH_
  
  class FaultNotifier;
  typedef FaultNotifier *FaultNotifier_ptr;
  
  typedef
    TAO_Objref_Var_T<
        FaultNotifier
      >
    FaultNotifier_var;
  
  typedef
    TAO_Objref_Out_T<
        FaultNotifier
      >
    FaultNotifier_out;

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:54

#if !defined (_FT_FAULTNOTIFIER_CH_)
#define _FT_FAULTNOTIFIER_CH_
  
  class TAO_FT_Export FaultNotifier
    : public virtual ::FT::PullMonitorable
  {
  public:
    friend class TAO::Narrow_Utils<FaultNotifier>;
    typedef FaultNotifier_ptr _ptr_type;
    typedef FaultNotifier_var _var_type;
    typedef FaultNotifier_out _out_type;
    
    // The static operations.
    static FaultNotifier_ptr _duplicate (FaultNotifier_ptr obj);
    
    static void _tao_release (FaultNotifier_ptr obj);
    
    static FaultNotifier_ptr _narrow (::CORBA::Object_ptr obj);
    static FaultNotifier_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
    static FaultNotifier_ptr _nil (void)
    {
      return static_cast<FaultNotifier_ptr> (0);
    }
    
    static void _tao_any_destructor (void *);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:379
    
    typedef ::CORBA::ULongLong ConsumerId;
    typedef ::CORBA::ULongLong_out ConsumerId_out;
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
    
    static ::CORBA::TypeCode_ptr const _tc_ConsumerId;
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void push_structured_fault (
        const ::CosNotification::StructuredEvent & event);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void push_sequence_fault (
        const ::CosNotification::EventBatch & events);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual ::CosNotifyFilter::Filter_ptr create_subscription_filter (
        const char * constraint_grammar);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual ::FT::FaultNotifier::ConsumerId connect_structured_fault_consumer (
        ::CosNotifyComm::StructuredPushConsumer_ptr push_consumer,
        ::CosNotifyFilter::Filter_ptr filter);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual ::FT::FaultNotifier::ConsumerId connect_sequence_fault_consumer (
        ::CosNotifyComm::SequencePushConsumer_ptr push_consumer,
        ::CosNotifyFilter::Filter_ptr filter);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_operation/operation_ch.cpp:46
    
    virtual void disconnect_consumer (
        ::FT::FaultNotifier::ConsumerId connection);
    
    // TAO_IDL - Generated from
    // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/interface_ch.cpp:216
    
    virtual ::CORBA::Boolean _is_a (const char *type_id);
    virtual const char* _interface_repository_id (void) const;
    virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
  private:
    TAO::Collocation_Proxy_Broker *the_TAO_FaultNotifier_Proxy_Broker_;
  
  protected:
    // Concrete interface only.
    FaultNotifier (void);
    
    // These methods travese the inheritance tree and set the
    // parents piece of the given class in the right mode.
    virtual void FT_FaultNotifier_setup_collocation (void);
    
    // Concrete non-local interface only.
    FaultNotifier (
        ::IOP::IOR *ior,
        TAO_ORB_Core *orb_core);
    
    // Non-local interface only.
    FaultNotifier (
        TAO_Stub *objref,
        ::CORBA::Boolean _tao_collocated = false,
        TAO_Abstract_ServantBase *servant = 0,
        TAO_ORB_Core *orb_core = 0);
    
    virtual ~FaultNotifier (void);
  
  private:
    // Private and unimplemented for concrete interfaces.
    FaultNotifier (const FaultNotifier &);
    
    void operator= (const FaultNotifier &);
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern TAO_FT_Export ::CORBA::TypeCode_ptr const _tc_FaultNotifier;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module FT

// Proxy Broker Factory function pointer declarations.

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_root/root.cpp:139

extern TAO_FT_Export
TAO::Collocation_Proxy_Broker *
(*FT__TAO_FaultNotifier_Proxy_Broker_Factory_function_pointer) (
    ::CORBA::Object_ptr obj
  );

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{

#if !defined (_FT_FAULTNOTIFIER__TRAITS_)
#define _FT_FAULTNOTIFIER__TRAITS_
  
  template<>
  struct TAO_FT_Export Objref_Traits< ::FT::FaultNotifier>
  {
    static ::FT::FaultNotifier_ptr duplicate (
        ::FT::FaultNotifier_ptr p
      );
    static void release (
        ::FT::FaultNotifier_ptr p
      );
    static ::FT::FaultNotifier_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::FT::FaultNotifier_ptr p,
        TAO_OutputCDR & cdr
      );
  };

#endif /* end #if !defined */
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/any_op_ch.cpp:54



#if defined (ACE_ANY_OPS_USE_NAMESPACE)

namespace FT
{
  TAO_FT_Export void operator<<= ( ::CORBA::Any &, FaultNotifier_ptr); // copying
  TAO_FT_Export void operator<<= ( ::CORBA::Any &, FaultNotifier_ptr *); // non-copying
  TAO_FT_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, FaultNotifier_ptr &);
}

#else



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_FT_Export void operator<<= (::CORBA::Any &, FT::FaultNotifier_ptr); // copying
TAO_FT_Export void operator<<= (::CORBA::Any &, FT::FaultNotifier_ptr *); // non-copying
TAO_FT_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, FT::FaultNotifier_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_interface/cdr_op_ch.cpp:55


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_FT_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const FT::FaultNotifier_ptr );
TAO_FT_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, FT::FaultNotifier_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#if defined (__ACE_INLINE__)
#include "FT_NotifierC.inl"
#endif /* defined INLINE */

#include /**/ "ace/post.h"

#endif /* ifndef */


