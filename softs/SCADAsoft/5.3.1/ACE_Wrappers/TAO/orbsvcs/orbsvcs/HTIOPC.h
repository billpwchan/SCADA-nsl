// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_IDL_HTIOPC_H_
#define _TAO_IDL_HTIOPC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "orbsvcs/HTIOP/HTIOP_Export.h"
#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/ORB.h"
#include "tao/Basic_Types.h"
#include "tao/String_Manager_T.h"
#include "tao/Sequence_T.h"
#include "tao/Seq_Var_T.h"
#include "tao/Seq_Out_T.h"
#include "tao/VarOut_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO HTIOP_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace HTIOP
{
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct ListenPoint;
  
  typedef
    TAO_Var_Var_T<
        ListenPoint
      >
    ListenPoint_var;
  
  typedef
    TAO_Out_T<
        ListenPoint
      >
    ListenPoint_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct HTIOP_Export ListenPoint
  {
    typedef ListenPoint_var _var_type;
    typedef ListenPoint_out _out_type;
    
    static void _tao_any_destructor (void *);
    TAO::String_Manager host;
    ::CORBA::UShort port;
    TAO::String_Manager htid;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern HTIOP_Export ::CORBA::TypeCode_ptr const _tc_ListenPoint;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_HTIOP_LISTENPOINTLIST_CH_)
#define _HTIOP_LISTENPOINTLIST_CH_
  
  class ListenPointList;
  
  typedef
    TAO_VarSeq_Var_T<
        ListenPointList
      >
    ListenPointList_var;
  
  typedef
    TAO_Seq_Out_T<
        ListenPointList
      >
    ListenPointList_out;
  
  class HTIOP_Export ListenPointList
    : public
        TAO::unbounded_value_sequence<
            ListenPoint
          >
  {
  public:
    ListenPointList (void);
    ListenPointList ( ::CORBA::ULong max);
    ListenPointList (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        ListenPoint* buffer, 
        ::CORBA::Boolean release = false
      );
    ListenPointList (const ListenPointList &);
    virtual ~ListenPointList (void);
    
    static void _tao_any_destructor (void *);
    
    typedef ListenPointList_var _var_type;
    typedef ListenPointList_out _out_type;
    
    
  };

#endif /* end #if !defined */
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern HTIOP_Export ::CORBA::TypeCode_ptr const _tc_ListenPointList;
  
  // TAO_IDL - Generated from
  // be\be_type.cpp:269
  
  struct BiDirHTIOPServiceContext;
  
  typedef
    TAO_Var_Var_T<
        BiDirHTIOPServiceContext
      >
    BiDirHTIOPServiceContext_var;
  
  typedef
    TAO_Out_T<
        BiDirHTIOPServiceContext
      >
    BiDirHTIOPServiceContext_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/structure_ch.cpp:57
  
  struct HTIOP_Export BiDirHTIOPServiceContext
  {
    typedef BiDirHTIOPServiceContext_var _var_type;
    typedef BiDirHTIOPServiceContext_out _out_type;
    
    static void _tao_any_destructor (void *);
    HTIOP::ListenPointList listen_points;
  };
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typecode/typecode_decl.cpp:49
  
  extern HTIOP_Export ::CORBA::TypeCode_ptr const _tc_BiDirHTIOPServiceContext;

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module HTIOP

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

HTIOP_Export void operator<<= (::CORBA::Any &, const HTIOP::ListenPoint &); // copying version
HTIOP_Export void operator<<= (::CORBA::Any &, HTIOP::ListenPoint*); // noncopying version
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, HTIOP::ListenPoint *&); // deprecated
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const HTIOP::ListenPoint *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

HTIOP_Export void operator<<= ( ::CORBA::Any &, const HTIOP::ListenPointList &); // copying version
HTIOP_Export void operator<<= ( ::CORBA::Any &, HTIOP::ListenPointList*); // noncopying version
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, HTIOP::ListenPointList *&); // deprecated
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const HTIOP::ListenPointList *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/any_op_ch.cpp:53



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

HTIOP_Export void operator<<= (::CORBA::Any &, const HTIOP::BiDirHTIOPServiceContext &); // copying version
HTIOP_Export void operator<<= (::CORBA::Any &, HTIOP::BiDirHTIOPServiceContext*); // noncopying version
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, HTIOP::BiDirHTIOPServiceContext *&); // deprecated
HTIOP_Export ::CORBA::Boolean operator>>= (const ::CORBA::Any &, const HTIOP::BiDirHTIOPServiceContext *&);
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

HTIOP_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const HTIOP::ListenPoint &);
HTIOP_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, HTIOP::ListenPoint &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_HTIOP_ListenPointList_H_
#define _TAO_CDR_OP_HTIOP_ListenPointList_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



HTIOP_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const HTIOP::ListenPointList &_tao_sequence
  );
HTIOP_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    HTIOP::ListenPointList &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_HTIOP_ListenPointList_H_ */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_structure/cdr_op_ch.cpp:54



TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

HTIOP_Export ::CORBA::Boolean operator<< (TAO_OutputCDR &, const HTIOP::BiDirHTIOPServiceContext &);
HTIOP_Export ::CORBA::Boolean operator>> (TAO_InputCDR &, HTIOP::BiDirHTIOPServiceContext &);

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#if defined (__ACE_INLINE__)
#include "HTIOPC.inl"
#endif /* defined INLINE */

#include /**/ "ace/post.h"

#endif /* ifndef */


