/* -*- c++ -*- */
//
// $Id: be_visitor_component_fwd.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_component_fwd.h
//
// = DESCRIPTION
//    Concrete visitor for the forward declared component class.
//
// = AUTHOR
//    Jeff Parsons
//
// ============================================================================

#ifndef TAO_BE_VISITOR_COMPONENT_FWD_H
#define TAO_BE_VISITOR_COMPONENT_FWD_H

#include "be_visitor_decl.h"

#include "be_visitor_component_fwd/component_fwd_ch.h"
#include "be_visitor_component_fwd/any_op_ch.h"
#include "be_visitor_component_fwd/cdr_op_ch.h"

#endif /* TAO_BE_VISITOR_COMPONENT_FWD_H */
