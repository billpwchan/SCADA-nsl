/**
 * @file PortableServer_include.pidl
 *
 * $Id: PortableServer_include.pidl 14 2007-02-01 15:49:12Z mitza $
 *
 * @brief Include file for use in applications that need PortableServer.pidl.
 *
 * This file just includes PortableServer.pidl. The *C.h file generated from
 * this is hand-crafted to itself include PortableServer.h instead of
 * PortableServerC.h (which will produce a compiler error message if
 * included directly). The PortableServer_includeC.h file can then be
 * included directly and automatically by the IDL compiler when
 * building the application.
 *
 *   1. Run the tao_idl compiler on the pidl file.  The command used for
 *     this is:
 *
 *     tao_idl -o orig -St -Sp -Sci -SS
 *          -Wb,export_macro=TAO_PortableServer_Export \
 *          -Wb,export_include="portableserver_export.h" \
 *          -Wb,pre_include="ace/pre.h"
 *          -Wb,post_include="ace/post.h"
 *          PortableServer_include.pidl
 *
 *   2. Then change this line in PortableServer_includeC.h:
 *
 *          #include "PortableServerC.h"
 *
 *      to
 *
 *          #include "PortableServer.h"
 */

#ifndef _PORTABLESERVER_INCLUDE_IDL_
#define _PORTABLESERVER_INCLUDE_IDL_

#pragma prefix ""

///FUZZ: disable check_for_include/
#include "tao/PortableServer/PortableServer.pidl"

#endif /* _PORTABLESERVER_INCLUDE_IDL_ */
