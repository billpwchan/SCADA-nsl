// -*- IDL -*-

/**
 * @file ORBInitInfo.pidl
 *
 * $Id: ORBInitInfo.pidl 935 2008-12-10 21:47:27Z mitza $
 *
 * @brief Pre-compiled IDL source for the ORBInitInfo
 *
 * This file is used to generate the code in ORBInitInfoC.*
 * The command used to generate code is:
 *
 *  tao_idl
 *     -o orig -Gp -Gd -SS -Sci
 *          -Wb,export_include="tao/TAO_Export.h"
 *          -Wb,export_macro=TAO_Export
 *          -Wb,pre_include="ace/pre.h"
 *          -Wb,post_include="ace/post.h"
 *          ORBInitInfo.pidl
 */

#ifndef _ORBINITINFO_PIDL_
#define _ORBINITINFO_PIDL_

#include "tao/StringSeq.pidl"
#include "tao/CodecFactory/IOP_Codec_include.pidl"
#include "tao/PI_Forward.pidl"
#include "tao/Policy.pidl"

module PortableInterceptor {

  typeprefix PortableInterceptor "omg.org";

  local interface ClientRequestInterceptor;
  local interface ServerRequestInterceptor;
  local interface IORInterceptor;
  local interface PolicyFactory;

  local interface ORBInitInfo
  {
    typedef string ObjectId;

    exception DuplicateName
    {
      string name;
    };

    exception InvalidName {};

    readonly attribute CORBA::StringSeq arguments;
    readonly attribute string orb_id;
    readonly attribute IOP::CodecFactory codec_factory;

    void register_initial_reference (in ObjectId id, in Object obj)
      raises (InvalidName);
    Object resolve_initial_references (in ObjectId id)
      raises (InvalidName);
    void add_client_request_interceptor (
      in ClientRequestInterceptor interceptor)
      raises (DuplicateName);
    void add_server_request_interceptor (
      in ServerRequestInterceptor interceptor)
      raises (DuplicateName);
    void add_ior_interceptor (in IORInterceptor interceptor)
      raises (DuplicateName);
    SlotId allocate_slot_id ();
    void register_policy_factory (
      in CORBA::PolicyType type,
      in PolicyFactory policy_factory);
  };

  local interface ORBInitInfo_3_1 : ORBInitInfo
  {
    void add_client_request_interceptor_with_policy(
       in ClientRequestInterceptor interceptor,
       in CORBA::PolicyList policies)
       raises (DuplicateName, CORBA::PolicyError);
    void add_server_request_interceptor_with_policy(
       in ServerRequestInterceptor interceptor,
       in CORBA::PolicyList policies)
       raises (DuplicateName, CORBA::PolicyError);
    void add_ior_interceptor_with_policy(
       in IORInterceptor interceptor,
       in CORBA::PolicyList policies)
       raises (DuplicateName, CORBA::PolicyError);
  };

};

#endif  /* _ORBINITINFO_PIDL_ */
