// -*- C++ -*-
//
// $Id$

// ****  Code generated by the The ACE ORB (TAO) IDL Compiler v1.6a_p10 ****
// TAO and the TAO IDL Compiler have been developed by:
//       Center for Distributed Object Computing
//       Washington University
//       St. Louis, MO
//       USA
//       http://www.cs.wustl.edu/~schmidt/doc-center.html
// and
//       Distributed Object Computing Laboratory
//       University of California at Irvine
//       Irvine, CA
//       USA
//       http://doc.ece.uci.edu/
// and
//       Institute for Software Integrated Systems
//       Vanderbilt University
//       Nashville, TN
//       USA
//       http://www.isis.vanderbilt.edu/
//
// Information about TAO is available at:
//     http://www.cs.wustl.edu/~schmidt/TAO.html

// TAO_IDL - Generated from
// be\be_codegen.cpp:135

#ifndef _TAO_PIDL_OBJECTIDLISTC_H_
#define _TAO_PIDL_OBJECTIDLISTC_H_

#include /**/ "ace/pre.h"


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include /**/ "tao/TAO_Export.h"
#include "tao/Basic_Types.h"
#include "tao/Sequence_T.h"
#include "tao/Seq_Var_T.h"
#include "tao/Seq_Out_T.h"
#include /**/ "tao/Versioned_Namespace.h"

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO TAO_Export

TAO_BEGIN_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:49

namespace CORBA
{
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_typedef/typedef_ch.cpp:413
  
  typedef char * ORB_ObjectId;
  typedef ::CORBA::String_var ORB_ObjectId_var;
  typedef ::CORBA::String_out ORB_ObjectId_out;
  
  // TAO_IDL - Generated from
  // d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/sequence_ch.cpp:107

#if !defined (_CORBA_ORB_OBJECTIDLIST_CH_)
#define _CORBA_ORB_OBJECTIDLIST_CH_
  
  class ORB_ObjectIdList;
  
  typedef
    TAO_VarSeq_Var_T<
        ORB_ObjectIdList
      >
    ORB_ObjectIdList_var;
  
  typedef
    TAO_Seq_Out_T<
        ORB_ObjectIdList
      >
    ORB_ObjectIdList_out;
  
  class TAO_Export ORB_ObjectIdList
    : public
        TAO::unbounded_basic_string_sequence<char>
  {
  public:
    ORB_ObjectIdList (void);
    ORB_ObjectIdList ( ::CORBA::ULong max);
    ORB_ObjectIdList (
        ::CORBA::ULong max,
        ::CORBA::ULong length,
        ::CORBA::Char ** buffer, 
        ::CORBA::Boolean release = false
      );
    ORB_ObjectIdList (const ORB_ObjectIdList &);
    virtual ~ORB_ObjectIdList (void);
    
    static void _tao_any_destructor (void *);
    
    typedef ORB_ObjectIdList_var _var_type;
    typedef ORB_ObjectIdList_out _out_type;
    
    
  };

#endif /* end #if !defined */

// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_module/module_ch.cpp:78

} // module CORBA

// TAO_IDL - Generated from
// be\be_visitor_traits.cpp:64


TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{
}
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




// TAO_IDL - Generated from
// d:\softs\ace_wrappers_vc10\tao\tao_idl\be\be_visitor_sequence/cdr_op_ch.cpp:71

#if !defined _TAO_CDR_OP_CORBA_ORB_ObjectIdList_H_
#define _TAO_CDR_OP_CORBA_ORB_ObjectIdList_H_

TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL



TAO_Export ::CORBA::Boolean operator<< (
    TAO_OutputCDR &strm,
    const CORBA::ORB_ObjectIdList &_tao_sequence
  );
TAO_Export ::CORBA::Boolean operator>> (
    TAO_InputCDR &strm,
    CORBA::ORB_ObjectIdList &_tao_sequence
  );
TAO_END_VERSIONED_NAMESPACE_DECL


TAO_BEGIN_VERSIONED_NAMESPACE_DECL




#endif /* _TAO_CDR_OP_CORBA_ORB_ObjectIdList_H_ */

// TAO_IDL - Generated from
// be\be_codegen.cpp:1228


TAO_END_VERSIONED_NAMESPACE_DECL

#include /**/ "ace/post.h"

#endif /* ifndef */


