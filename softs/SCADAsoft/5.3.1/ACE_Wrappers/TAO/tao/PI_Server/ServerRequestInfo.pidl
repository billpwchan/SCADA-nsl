// -*- IDL -*-

/**
 * @file ServerRequestInfo.pidl
 *
 * $Id: ServerRequestInfo.pidl 935 2008-12-10 21:47:27Z mitza $
 *
 * @brief Pre-compiled RequestInfo
 *
 * This file is used to generate the code in ServerRequestInfoC.*
 * The command used to generate code is:
 *
 *  tao_idl
 *     -o orig -Gp -Gd -GA -SS -Sci
 *          -Wb,export_include="tao/TAO_Export.h"
 *          -Wb,export_macro=TAO_Export
 *          -Wb,pre_include="ace/pre.h"
 *          -Wb,post_include="ace/post.h"
 *          ServerRequestInfo.pidl
 */

#ifndef _SERVER_REQUESTINFO_PIDL_
#define _SERVER_REQUESTINFO_PIDL_

#include "tao/PI/PI_include.pidl"
#include "tao/orb_types.pidl"
#include "tao/Policy_Forward.pidl"

module PortableInterceptor {

  typeprefix PortableInterceptor "omg.org";

  local interface ServerRequestInfo : RequestInfo
  {
    readonly attribute any sending_exception;
    readonly attribute ServerId server_id;
    readonly attribute ORBId orb_id;
    readonly attribute AdapterName adapter_name;
    readonly attribute ObjectId object_id;
    readonly attribute CORBA::OctetSeq adapter_id;
    readonly attribute CORBA::RepositoryId target_most_derived_interface;
    CORBA::Policy get_server_policy (in CORBA::PolicyType type);
    void set_slot (in SlotId id, in any data) raises (InvalidSlot);
    boolean target_is_a (in CORBA::RepositoryId id);
    void add_reply_service_context (
      in IOP::ServiceContext service_context,
      in boolean replace);
  };
};

#endif  /* _SERVER_REQUESTINFO_PIDL_ */
