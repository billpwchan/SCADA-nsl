var NAVTREE =
[
  [ "SCADAsoft-APIs", "index.html", [
    [ "ScadaSoft-APIs documentation", "index.html", null ],
    [ "Class List", "annotated.html", [
      [ "AscAgent", "classAscAgent.html", null ],
      [ "AscManager", "classAscManager.html", null ],
      [ "AscServer", "classAscServer.html", null ],
      [ "AscSNMPAgent", "classAscSNMPAgent.html", null ],
      [ "AscStateMgr", "classAscStateMgr.html", null ],
      [ "CtlCmdCommand", "classCtlCmdCommand.html", null ],
      [ "CtlCmdServer", "classCtlCmdServer.html", null ],
      [ "CtlGrc", "classCtlGrc.html", null ],
      [ "CtlGrcServer", "classCtlGrcServer.html", null ],
      [ "DacEIV", "classDacEIV.html", null ],
      [ "DacEOV", "classDacEOV.html", null ],
      [ "DacServer", "classDacServer.html", null ],
      [ "DacSimulator", "classDacSimulator.html", null ],
      [ "DbkSession", "classDbkSession.html", null ],
      [ "DbmData", "classDbmData.html", null ],
      [ "DbmDataSet", "classDbmDataSet.html", null ],
      [ "DbmDataSetIterator", "classDbmDataSetIterator.html", null ],
      [ "DbmDirectData", "classDbmDirectData.html", null ],
      [ "DbmDirectDataListIterator", "classDbmDirectDataListIterator.html", null ],
      [ "DbmDirectDataSet", "classDbmDirectDataSet.html", null ],
      [ "DbmPoller", "classDbmPoller.html", null ],
      [ "DbmServer", "classDbmServer.html", null ],
      [ "DpcEqpServer", "classDpcEqpServer.html", null ],
      [ "FtsServer", "classFtsServer.html", null ],
      [ "HisDataAddress", "classHisDataAddress.html", null ],
      [ "HisDbData", "classHisDbData.html", null ],
      [ "HisDbDataSet", "classHisDbDataSet.html", null ],
      [ "HisDbDataSetIt", "classHisDbDataSetIt.html", null ],
      [ "HisDbDataSetList", "classHisDbDataSetList.html", null ],
      [ "HisPoint", "classHisPoint.html", null ],
      [ "HisPointSet", "classHisPointSet.html", null ],
      [ "HisPointSetIt", "classHisPointSetIt.html", null ],
      [ "HisPointSetList", "classHisPointSetList.html", null ],
      [ "HisQuery", "classHisQuery.html", null ],
      [ "HisReadFileMgr", "classHisReadFileMgr.html", null ],
      [ "HisServer", "classHisServer.html", null ],
      [ "HisValue", "classHisValue.html", null ],
      [ "LstServer", "classLstServer.html", null ],
      [ "OlsAndCriteria", "classOlsAndCriteria.html", null ],
      [ "OlsCollectableList", "classOlsCollectableList.html", null ],
      [ "OlsCriteria", "classOlsCriteria.html", null ],
      [ "OlsDataSet", "classOlsDataSet.html", null ],
      [ "OlsDataSetIterator", "classOlsDataSetIterator.html", null ],
      [ "OlsField", "classOlsField.html", null ],
      [ "OlsFieldCriteria", "classOlsFieldCriteria.html", null ],
      [ "OlsFilter", "classOlsFilter.html", null ],
      [ "OlsLimitedList", "classOlsLimitedList.html", null ],
      [ "OlsList", "classOlsList.html", null ],
      [ "OlsListWithClientVisibility", "classOlsListWithClientVisibility.html", null ],
      [ "OlsManager", "classOlsManager.html", null ],
      [ "OlsNotCriteria", "classOlsNotCriteria.html", null ],
      [ "OlsOrCriteria", "classOlsOrCriteria.html", null ],
      [ "OlsQuickList", "classOlsQuickList.html", null ],
      [ "OlsServer", "classOlsServer.html", null ],
      [ "OpmApi", "classOpmApi.html", null ],
      [ "RplRecordMgr", "classRplRecordMgr.html", null ],
      [ "RplRecordServer", "classRplRecordServer.html", null ],
      [ "RplRecordServerFilesInfo", "classRplRecordServerFilesInfo.html", null ],
      [ "RplRecordServerInfo", "classRplRecordServerInfo.html", null ],
      [ "RplReplay", "classRplReplay.html", null ],
      [ "RplReplayMgr", "classRplReplayMgr.html", null ],
      [ "RplReplayMgrFilesInfo", "classRplReplayMgrFilesInfo.html", null ],
      [ "RplReplayMgrInfo", "classRplReplayMgrInfo.html", null ],
      [ "ScadaORB", "classScadaORB.html", null ],
      [ "Scadasoft", "classScadasoft.html", null ],
      [ "ScsAddress", "classScsAddress.html", null ],
      [ "ScsAlmExtern", "classScsAlmExtern.html", null ],
      [ "ScsAlmServer", "classScsAlmServer.html", null ],
      [ "ScsData", "classScsData.html", null ],
      [ "ScsMedia", "classScsMedia.html", null ],
      [ "ScsTimer", "classScsTimer.html", null ],
      [ "ScsTimeZone", "classScsTimeZone.html", null ],
      [ "ScsWdg", "classScsWdg.html", null ],
      [ "SecServer", "classSecServer.html", null ],
      [ "TrdData", "classTrdData.html", null ],
      [ "TrdDataSet", "classTrdDataSet.html", null ],
      [ "TrdHisSubscription", "classTrdHisSubscription.html", null ],
      [ "TrdServer", "classTrdServer.html", null ],
      [ "TrdSubscriber", "classTrdSubscriber.html", null ],
      [ "TscServer", "classTscServer.html", null ],
      [ "TscTaskArg", "classTscTaskArg.html", null ],
      [ "TscTaskCmdArg", "classTscTaskCmdArg.html", null ],
      [ "TscTaskEvtArg", "classTscTaskEvtArg.html", null ],
      [ "TscTaskExecArg", "classTscTaskExecArg.html", null ],
      [ "TscTaskGrcArg", "classTscTaskGrcArg.html", null ]
    ] ],
    [ "Class Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "AscAgent", "classAscAgent.html", null ],
      [ "AscManager", "classAscManager.html", null ],
      [ "AscServer", "classAscServer.html", null ],
      [ "AscSNMPAgent", "classAscSNMPAgent.html", null ],
      [ "AscStateMgr", "classAscStateMgr.html", null ],
      [ "CtlCmdCommand", "classCtlCmdCommand.html", null ],
      [ "CtlCmdServer", "classCtlCmdServer.html", null ],
      [ "CtlGrc", "classCtlGrc.html", null ],
      [ "CtlGrcServer", "classCtlGrcServer.html", null ],
      [ "DacEIV", "classDacEIV.html", null ],
      [ "DacEOV", "classDacEOV.html", null ],
      [ "DacServer", "classDacServer.html", null ],
      [ "DacSimulator", "classDacSimulator.html", null ],
      [ "DbkSession", "classDbkSession.html", null ],
      [ "DbmData", "classDbmData.html", null ],
      [ "DbmDataSet", "classDbmDataSet.html", null ],
      [ "DbmDataSetIterator", "classDbmDataSetIterator.html", null ],
      [ "DbmDirectData", "classDbmDirectData.html", null ],
      [ "DbmDirectDataListIterator", "classDbmDirectDataListIterator.html", null ],
      [ "DbmDirectDataSet", "classDbmDirectDataSet.html", null ],
      [ "DbmPoller", "classDbmPoller.html", null ],
      [ "DbmServer", "classDbmServer.html", null ],
      [ "DpcEqpServer", "classDpcEqpServer.html", null ],
      [ "FtsServer", "classFtsServer.html", null ],
      [ "HisDataAddress", "classHisDataAddress.html", null ],
      [ "HisDbData", "classHisDbData.html", null ],
      [ "HisDbDataSet", "classHisDbDataSet.html", null ],
      [ "HisDbDataSetIt", "classHisDbDataSetIt.html", null ],
      [ "HisDbDataSetList", "classHisDbDataSetList.html", null ],
      [ "HisPoint", "classHisPoint.html", null ],
      [ "HisPointSet", "classHisPointSet.html", null ],
      [ "HisPointSetIt", "classHisPointSetIt.html", null ],
      [ "HisPointSetList", "classHisPointSetList.html", null ],
      [ "HisQuery", "classHisQuery.html", null ],
      [ "HisReadFileMgr", "classHisReadFileMgr.html", null ],
      [ "HisValue", "classHisValue.html", [
        [ "TrdData", "classTrdData.html", null ]
      ] ],
      [ "LstServer", "classLstServer.html", null ],
      [ "OlsCriteria", "classOlsCriteria.html", [
        [ "OlsAndCriteria", "classOlsAndCriteria.html", null ],
        [ "OlsFieldCriteria", "classOlsFieldCriteria.html", null ],
        [ "OlsNotCriteria", "classOlsNotCriteria.html", null ],
        [ "OlsOrCriteria", "classOlsOrCriteria.html", null ]
      ] ],
      [ "OlsDataSet", "classOlsDataSet.html", null ],
      [ "OlsDataSetIterator", "classOlsDataSetIterator.html", null ],
      [ "OlsField", "classOlsField.html", null ],
      [ "OlsFilter", "classOlsFilter.html", null ],
      [ "OlsList", "classOlsList.html", [
        [ "OlsCollectableList", "classOlsCollectableList.html", null ],
        [ "OlsLimitedList", "classOlsLimitedList.html", null ],
        [ "OlsListWithClientVisibility", "classOlsListWithClientVisibility.html", null ],
        [ "OlsQuickList", "classOlsQuickList.html", null ]
      ] ],
      [ "OlsManager", "classOlsManager.html", null ],
      [ "OlsServer", "classOlsServer.html", [
        [ "HisServer", "classHisServer.html", [
          [ "TrdServer", "classTrdServer.html", null ]
        ] ],
        [ "ScsAlmServer", "classScsAlmServer.html", null ]
      ] ],
      [ "OpmApi", "classOpmApi.html", null ],
      [ "RplRecordMgr", "classRplRecordMgr.html", null ],
      [ "RplRecordServer", "classRplRecordServer.html", null ],
      [ "RplRecordServerFilesInfo", "classRplRecordServerFilesInfo.html", null ],
      [ "RplRecordServerInfo", "classRplRecordServerInfo.html", null ],
      [ "RplReplay", "classRplReplay.html", null ],
      [ "RplReplayMgr", "classRplReplayMgr.html", null ],
      [ "RplReplayMgrFilesInfo", "classRplReplayMgrFilesInfo.html", null ],
      [ "RplReplayMgrInfo", "classRplReplayMgrInfo.html", null ],
      [ "ScadaORB", "classScadaORB.html", null ],
      [ "Scadasoft", "classScadasoft.html", null ],
      [ "ScsAddress", "classScsAddress.html", null ],
      [ "ScsAlmExtern", "classScsAlmExtern.html", null ],
      [ "ScsData", "classScsData.html", null ],
      [ "ScsMedia", "classScsMedia.html", null ],
      [ "ScsTimer", "classScsTimer.html", null ],
      [ "ScsTimeZone", "classScsTimeZone.html", null ],
      [ "ScsWdg", "classScsWdg.html", null ],
      [ "SecServer", "classSecServer.html", null ],
      [ "TrdDataSet", "classTrdDataSet.html", null ],
      [ "TrdHisSubscription", "classTrdHisSubscription.html", null ],
      [ "TrdSubscriber", "classTrdSubscriber.html", null ],
      [ "TscServer", "classTscServer.html", null ],
      [ "TscTaskArg", "classTscTaskArg.html", [
        [ "TscTaskCmdArg", "classTscTaskCmdArg.html", null ],
        [ "TscTaskEvtArg", "classTscTaskEvtArg.html", null ],
        [ "TscTaskExecArg", "classTscTaskExecArg.html", null ],
        [ "TscTaskGrcArg", "classTscTaskGrcArg.html", null ]
      ] ]
    ] ],
    [ "Class Members", "functions.html", null ],
    [ "File List", "files.html", [
      [ "agent.h", "agent_8h.html", null ],
      [ "asc.h", "asc_8h.html", null ],
      [ "ascsnmpagent.h", "ascsnmpagent_8h.html", null ],
      [ "ctlcmd.h", "ctlcmd_8h.html", null ],
      [ "ctlgrc.h", "ctlgrc_8h.html", null ],
      [ "dac.h", "dac_8h.html", null ],
      [ "dacsim.h", "dacsim_8h.html", null ],
      [ "dbm.h", "dbm_8h.html", null ],
      [ "dbmdirect.h", "dbmdirect_8h.html", null ],
      [ "dbmhistory.h", "dbmhistory_8h.html", null ],
      [ "dpc.h", "dpc_8h.html", null ],
      [ "fts.h", "fts_8h.html", null ],
      [ "his.h", "his_8h.html", null ],
      [ "hisreadfilemgr.h", "hisreadfilemgr_8h.html", null ],
      [ "histypes.h", "histypes_8h.html", null ],
      [ "lst.h", "lst_8h.html", null ],
      [ "manager.h", "manager_8h.html", null ],
      [ "ols.h", "ols_8h.html", null ],
      [ "olsactivation.h", "olsactivation_8h.html", null ],
      [ "olstypes.h", "olstypes_8h.html", null ],
      [ "opmapi.h", "opmapi_8h.html", null ],
      [ "scadaorb.h", "scadaorb_8h.html", null ],
      [ "scs.h", "scs_8h.html", null ],
      [ "scsalm.h", "scsalm_8h.html", null ],
      [ "scsalmext.h", "scsalmext_8h.html", null ],
      [ "scsdata.h", "scsdata_8h.html", null ],
      [ "scsdefs.h", "scsdefs_8h.html", null ],
      [ "scsmedia.h", "scsmedia_8h.html", null ],
      [ "scsrpl.h", "scsrpl_8h.html", null ],
      [ "scstimer.h", "scstimer_8h.html", null ],
      [ "scstimezone.h", "scstimezone_8h.html", null ],
      [ "scsutils.h", "scsutils_8h.html", null ],
      [ "scswdg.h", "scswdg_8h.html", null ],
      [ "sec.h", "sec_8h.html", null ],
      [ "trd.h", "trd_8h.html", null ],
      [ "trddata.h", "trddata_8h.html", null ],
      [ "trddataset.h", "trddataset_8h.html", null ],
      [ "trdhissubscription.h", "trdhissubscription_8h.html", null ],
      [ "tsc.h", "tsc_8h.html", null ],
      [ "tsctypes.h", "tsctypes_8h.html", null ],
      [ "Types.h", "Types_8h.html", null ],
      [ "dbk/dbksession.h", "dbksession_8h.html", null ]
    ] ],
    [ "Directories", "dirs.html", [
      [ "dbk", "dir_c2763bf0f07e4b34cf3e00002c96de71.html", [
        [ "dbksession.h", "dbksession_8h.html", null ]
      ] ],
      [ "doxygen", "dir_990e5b0e46c5df08c832adf8f949a02a.html", [
        [ "public", "dir_d3e166c5f880d3f5e910ed8a438f499f.html", [
          [ "publicMain.h", "publicMain_8h.html", null ]
        ] ]
      ] ]
    ] ],
    [ "File Members", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

